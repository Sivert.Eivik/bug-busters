# Rapport – innlevering 3
**Team:** BugBusters – Sivert Sundvold Eivik, Andreas Lauritzen, Ingeborg Øksnes, Eirill Øya

# Hvordan fungerer rollene i teamet? Trenger dere å oppdatere hvem som er teamlead eller kundekontakt?
Vi endret rollene til forrige oblig, og rollene fungerer bra. Vi føler ikke noe behov for å endre rollene fremover. 

# Trenger dere andre roller? Skriv ned noen linjer om hva de ulike rollene faktisk innebærer for dere.
Vi føler ikke noe behov for andre roller. Som nevnt tidligere, så har vi mer fokus på ansvarsområder. 

# Er det noen erfaringer enten team-messig eller mtp prosjektmetodikk som er verdt å nevne? Synes teamet at de valgene dere har tatt er gode? Hvis ikke, hva kan dere gjøre annerledes for å forbedre måten teamet fungerer på?
Siden sist har vi valgt å gjøre det mer oversiktlig ved å bruke en nettside for å sette opp kanban board. Her har vi full oversikt over hvem som skal gjøre hva, hvilke oppgaver som skal fullføres, status for oppgaven som skal gjennomføres osv. 
Vi syns valgene vi har tatt har vært gode. Det har vært enkelt å vite hva man skal jobbe med, i tillegg til å vite hva som må gjøres for å komme videre på prosjektet.
Link til kanban board:
https://bugbusters-ensemble.monday.com/boards/1455878126
Si ifra dersom linken ikke fungerer. 


# Hvordan er gruppedynamikken? Er det uenigheter som bør løses?
Gruppedynamikken er god. Vi diskuterer bra og ser alle sine synspunkter for at alle skal være delaktig. Har frem til nå ikke vært noen uenigheter, mye fordi vi har diskutert og planlagt før vi begynner på arbeidsoppgaver.

# Hvordan fungerer kommunikasjonen for dere?
Kommunikasjonen fungerer bra. Vi bruker fremdeles Discord og Snapchat for å kommunisere, og alle følger med på alt som blir sendt. 

# Gjør et kort retrospektiv hvor dere vurderer hva dere har klart til nå, og hva som kan forbedres. Dette skal handle om prosjektstruktur, ikke kode. Dere kan selvsagt diskutere kode, men dette handler ikke om feilretting, men om hvordan man jobber og kommuniserer.
Tidligere ville vi forbedre commits til main i forhold til bedre commits og commit meldinger. Dette føler vi at har forbedret seg. Alle skriver commits på norsk, og det er tydelige oppdateringer for hva som blir gjort. Vi er også blitt flinkere til å ikke pushe til main før oppgaven er så og si ferdigstilt. 
Uten om dette er vi veldig fornøyd med prosjektet sett i retrospekt. Samarbeidet fungerer bra og vi hjelper hverandre.

# Bli enige om maks tre forbedringspunkter fra retrospektivet, som skal følges opp under neste sprint.
* Vi har gjenbrukt kode nå for å se at ting fungerer uten å påvirke andre elemeneter. Til neste innlevering ønsker vi å kutte ned på gjenbruken av kode. 

# Oppdater hvilke krav dere har prioritert, hvor langt dere har kommet og hva dere har gjort siden forrige gang. Er dere kommet forbi MVP? Forklar hvordan dere prioriterer ny funksjonalitet.
Nå har vi fått inn alle elementene fra MVP, men med noen bugs. Vi har altså valgt å prioritere å få inn alle elementene for å lage et spill, og heller rette bugs til neste innlevering. 
Måten vi prioriterer ny funksjonalitet er ved å legge inn nye elementer så godt det lar seg gjøre, og heller rette små bugs som f.eks. animasjon senere, så lenge det ikke påvirker andre elementer i stor grad. Dette er for å få en fremgang i prosjektet og ikke bli "stuck" på et punkt. 

# Forklar kort hvordan dere har prioritert oppgavene fremover
Vi har kommet forbi MVP, og ønsker derfor å fokusere på å feilrette bugs for å gjøre spillet komplett. Evt legge til ekstra elementer som flere maps. 

# Har dere gjort justeringer på kravene som er med i MVP? Forklar i så fall hvorfor. Hvis det er gjort endringer i rekkefølge utfra hva som er gitt fra kunde, hvorfor er dette gjort?
MVPen vi beskrev i Oblig 1 har blitt jobbet etter, og vi har ikke gjort noen justeringer. 

# Oppdater hvilke krav dere har prioritert, hvor langt dere har kommet og hva dere har gjort siden forrige gang.
Kravene vi har prioritert er å ferdigstille MVP, dvs. legge inn fiender med animasjon, fireball, score osv.
I tillegg til dette har vi prioritert tester. 

# Husk å skrive hvilke bugs som finnes i de kravene dere har utført (dersom det finnes bugs).
Bugs vi har nå:
* Spiller animasjon - når spilleren beveger seg er ikke alltid animasjonen helt riktig
* Pauseknappen har ikke dynamisk posisjon på skjermen. Dvs at når spilleren bevegeser seg og kamera endrer posisjon, følger ikke pauseknappen med.
* Det er en bug når det kommer til den ene testen testBeginContactWithEnemy(). Denne kjørte og ble godkjent før vi merget, men nå vil denne ikke kjøre. Grunnen til dette er at mocken ikke skjer. Dette skal vi løse til neste gang.
* Det er en bug når testen testJumpingStateWhenAbleToJump() kjøres, fordi gameSounds er null. Dette skal vi fikse da vi skal endre på logikken for når gameSounds.jump() skal brukes. Hvis gameSound fjernes i controller passerer testen.
