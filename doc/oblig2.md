# Rapport – innlevering 2
**Team:** BugBusters – Sivert Sundvold Eivik, Andreas Lauritzen, Ingeborg Øksnes, Eirill Øya

*Prosjektrapport*

# Hvordan fungerer rollene i teamet?
Rollene i teamet har endret seg litt siden start. Sivert har gjort en god jobb som gruppeleder så han fortsetter som det, men vi andre har tatt på oss litt andre arberidsoppgaver. Eirill har tatt mer ansvar for testene. Andreas og Ingeborg har både jobbet med backend og frontend. Derfor har vi valgt å endre rollene til at Eirill er testansvarlig, Ingeborg er backend og Andreas blir frontend. Ellers er det verdt å nevne at alle jobber ganske flytende med forskjellige oppgaver og at det ikke er noen veldig tydelige roller. 

# Trenger dere andre roller?
De ulike rollene handler for oss mest om hva man jobber mest med, altså hvilke arbeisoppgaver man har i prosjektet. Vi har hatt et større fokus på å gi forkjellige ansvarsområder i større grad enn å ha roller. F. eks. har Ingeborg hatt ansvar for meny og liknende, Andreas har hatt ansvar for animasjoner og spilleren, Sivert har hatt ansvar for map, gravitasjon, enemy og håndtering av gamestates. Eirill har hatt ansvar for tester og enemy. 

# Er det noen erfaringer enten team-messig eller mtp prosjektmetodikk som er verdt å nevne?
Vi har flere møter i uka som hjelper oss med å se hvordan ting ligger an og hvilke oppgaver som må gjøres fra uke til uke. Alle møter alltid opp på disse som gjør at alle alltid har god oversikt over hvordan vi ligger an i prosjektet. 

Vi syns at vi har lagt opp prosjektet på en god måte, hvor vi valgte å bruke køsystem med blokkering fra kanban. Først laget et map, deretter en spiller, så en enemy, osv. Dette har fungert veldig bra og vi er fornøyd med at vi gjorde dette og gjennomførte det på en god måte. Har vært ting som kan forbedres underveis uten at det er perfekt, men vi har ikke gått videre fra noe uferdig. 

Vi har jobbet mest individuelt, og heller spurt om hjelp og sammarbeidet på noen temaer. Men det er kanskje mulig og lurt å jobbe mer to og to, eller flere sammen i parkoding og liknende.

# Hvordan er gruppedynamikken? Er det uenigheter som bør løses?
Det har fungert veldig bra, alle har fått oppgaver de selv vil gjøre. Vi har vært enig om strukturen for prosjektet og alle har fått deltatt i planleggingen. 

# Hvordan fungerer kommunikasjonen for dere?
Vi har flere plattformer vi kommuniserer. Vi bruker Snapchat for å kommunisere om møter, og discord for å dele lenker, bilder, kode osv. Vi sitter også mye sammen på skolen både når vi jobber med INF112 og andre fag som gjør at vi også lett kan kommunisere om ting i hverdagen. 

# Gjør et kort retrospektiv hvor dere vurderer hva dere har klart til nå, og hva som kan forbedres.
Det som fungerer bra er at alle klarer sine arbeidsoppgaver på en god måte. Når noe er vanskelig går vi sammen og diskuterer hvordan det kan løses og også i noen tilfeller koder sammen. Vi er kodemessig på et veldig likt nivå. Vi har nådd de målene vi har satt, blant annet at vi skulle ha en MVP til oblig2. 

Alle har hver sin branch hvor man jobber og får gjort sine deler. Men når det kommer til main branchen kan vi ha enda bedre commits og spesielt commit messages.

# Bli enige om maks tre forbedringspunkter fra retrospektivet, som skal følges opp under neste sprint.
 * Mer kontroll over git, når det kommer til push og commit. 
 * Enda bedre mappe struktur, spesielt å få en del kode inn i model og ikke ha det i upassende filer/klasser

*Krav og spesifikasjon*

# Oppdater hvilke krav dere har prioritert, hvor langt dere har kommet og hva dere har gjort siden forrige gang. Er dere kommet forbi MVP?
Vi har prioritert å bli ferdig med MVP, med unntak score. Det er selvsagt forbedringspotensiale på flere av punktene i MVP men vi fikk til det som var målet. Forrige gang hadde vi så vidt noe kode, så alt som er implementert er nytt. Siden vi benytter oss av Box2D har vi ingen kode fra oblig1 igjen. 

# Forklar kort hvordan dere har prioritert oppgavene fremover
Bygge videre og forbedre mange av de aspektene vi allerede har begynt på. Ønsker også til neste innlevering å ha et score-system, samt mulighet for power-ups/upgrades for spilleren. Nå som vi har fått vår MVP ønsker vi å jobbe tettere sammen for å forbedre de ulike aspektene. F. eks. ved å bruke mer parprogrammering. 

# Har dere gjort justeringer på kravene som er med i MVP? Forklar i så fall hvorfor.
Nei, har ikke endret kravene. Men vi er klar over at vi ikke har laget score og power-ups. 

# Oppdater hvilke krav dere har prioritert, hvor langt dere har kommet og hva dere har gjort siden forrige gang.
Vi har prioritert map, spiller og meny. Disse punktene er det som er mest ferdigstilt, mens sånn som enemy er noe vi må jobbe videre med. 

# Husk å skrive hvilke bugs som finnes i de kravene dere har utført
* Spilleranimasjonene er ikke riktige i forhold til hva bevegelsene til spilleren er. 
* Bakgrunnsbildet til menyen kan bli lite ved noen anledninger hvis man har storskjerm. 
* Har en bug når man rendrer et map større enn vinduet som blir vist. 


