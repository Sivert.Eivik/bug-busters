## Dato
15. februar 

## Tilstede
Alle (Andreas, Ingeborg, Sivert, Eirill)

## Dette møtet
Ferdigstiller oblig1 og leverer den. Til neste gang ønsker vi å få laget et tilemap og implementere den i koden, og prøve å få karaketeren til å interagere med mappet. Neste møte blir tirsdag 20.feb i gruppetimen. Da ønsker vi å begynne å se på Box2D og få karakteren til å hoppe og bli kjent med det verktøyet. Til da skal alle lese seg opp og prøve å skaffe seg litt kunnskap om Box2D, så vi ikke trenger å bruke tiden på møtet til det. Mangler å lage tag til oblig1, skal få snakket med gruppeleeder og laget det før i morgen (fredag). 

test