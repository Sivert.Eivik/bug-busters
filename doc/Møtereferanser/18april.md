## Dato
18. april 

## Tilstede
Alle (Andreas, Ingeborg, Sivert, Eirill)

## Dette møtet
På dette møtet ble vi enige om å ikke legge til nye elemter til spillet, men heller fokusere på å forbedre det vi allerede har. Alle skal gjøre småjusteringer i ormådet de har ansvar for. Dersom noen trenger hjelp med sitt området så hjelper alle til. Vi skal jobbe med dette frem til neste møte, tirsdag 23 april. 