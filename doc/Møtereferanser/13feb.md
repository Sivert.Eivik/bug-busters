## Dato
13. februar 

## Tilstede
Alle (Andreas, Ingeborg, Sivert, Eirill)

## Dette møtet
Fortsetter med oblig 1, A4 og A5. Har bestemt oss for "prosjektoppsett", ønsker å bruke model, view, controller. Forhørt oss med gruppeleder, og bestemt oss for å bruke tiled (https://www.mapeditor.org) for å lage spillbrettet vårt. Vi har startet å prøve å skrive kode, alle har lest seg opp på libGDX og prøver seg frem. Målet innen fredag (16.feb) er at vi har en karakter som kan bevege seg over spillbrettet. Vi fant en foreløpig bakgrunn og karakter, og klarte og få karaketren til å bevege seg over spillbrettet ved hjelp av piltastene og noe endring på karakter når vi skifter retning osv. Til neste gang har vi fordelt noen oppgaver; Eirill skal jobbe med å få inn en fiende, Ingeborg skal prøve å få karaketern til å flytte seg hele tiden når vi holder inn piltasten og ikke bare hver gang vi trykker.
Skal ha neste møte på torsdag da vi skal gjøre A5, og ferdigstille oblig1.
