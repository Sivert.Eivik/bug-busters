## Dato
15. februar 

## Tilstede
Alle (Andreas, Ingeborg, Sivert, Eirill)

## Dette møtet
Sivert og Andreas hadde sett på Box2D, og de hadde prøvd å implementere det. Men dette viste seg å være veldig vanskelig, så vi bestemte oss for å ikke bruke Box2D. Derfor skal vi heller prøve å løse det på en annen måte. Vi har heller lyst til å implementere noe selv. Sivert lagde TileMap, som han pushet.
Vi lagde en branch hver i Git. Dette hadde vi ikke gjort før. Dette skal forhåpentligvis gjøre det lettere å pushe og pulle.
Vi tenker at det er ulike hovedansvarsoppgaver: startmeny og sluttmeny, og de andre skal håndtere spilleren og alt rundt det.
Ingeborg og Eirill skal lage start- og sluttmeny. Andreas og Sivert skal lage en spiller som interagerer med spillbrettet. Vi tenker å sette av hele torsdagen, 22. feb, til å jobbe med dette. Vi skal også se på det litt før torsdag.