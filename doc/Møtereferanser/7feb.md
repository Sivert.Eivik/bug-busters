## Dato
6. februar 

## Tilstede
Alle (Andreas, Ingeborg, Sivert, Eirill)

## Dette møtet
Vi kom frem til hva slags spill vi ønsket å lage. Vi ble enige om et liknende spill som Super Mario. Deretter gjorde vi ferdig oppgave A1, og oppgave A2. Vi var enige om hva vi ønsket å gjøre med spillet. Likevel tenker vi å se hva vi får til underveis og bygge på det vi har allerede skrevet. Deretter gjorde vi halve A3. Og vi tenker å gjøre resten i løpet av uken når vi får hjelp av gruppeleder til å forstå møtereferanser. 