## Dato
6. februar 

## Tilstede
Alle (Andreas, Ingeborg, Sivert, Eirill)

## Dette møtet

## Oppgave A0

Vi diskuterte hvilket av prosjektene vi skulle forke. Vi kom frem til libgdx. Dette var en felles beslutning vi tok, fordi vi fikk veiledning av gruppeleder. I tillegg gjorde vi oppgave A0. Siden gruppen har godt kjennskap til hverandre fra før så skrev vi en felles oppsummering. Deretter sendte alle en liten beskrivelse av seg selv og sine spesialiteter.  

## Oppgave A1

Sivert lagde en gruppe på Git. Han la til gruppemedlemmene, gruppeleder og foreleser. 
Vi diskuterte hva slags roller alle gruppemedlemmene skulle ha. Vi kom frem til at vi skal ha teamleder, backend, frontend og test-ansvarlig. Vi ble enige om rollene sammen. Likevel tenker vi at alle samarbeider på tvers av rollene, men vi har ekstra ansvar for hver av rollene.  

## Oppgave A2

Vi bestemte oss for å lage en liste med forslag av spill vi tenker å lage. Ut ifra denne listen skal vi ta en beslutning 7. februar på hva skal spill vi skal lage. 

## Hva vi skal gjøre til neste gang

Vi tenker å bruke litt tid på å finne ut av hva slags spill vi vi lage. Vi skal se på listen og deretter bestemme oss for hva vi ønsker å lage. Dette skal vi se på 7. februar. Da burde vi gjøre oppgave A2 og A3. Deretter bruker vi Gruppetimen neste uke på oppgave A4 og A5. Vi tenker i hvert fall å bruke gruppetimen vår til møter. 