## Dato
12. mars

## Tilstede
Alle (Andreas, Ingeborg, Sivert, Eirill)

## Dette møtet
Vi gikk gjennom hva alle har jobbet med og laget, og sjekket at alt vi hadde pushet til main for MVP var riktig. Vi bestemte oss for å senke tempet litt til påske til fordel for andre fag, og jobbe mer intensivt etter påske. Til neste gang skal Eirill fortsette med å lage tester, Sivert skal jobbe med å lage et nytt og lengre map, Andreas skal jobbe videre med å ferdigstille animasjoner til player og Ingeborg skal jobbe videre med å lage menuScreen og implementere bilder på en mer optimal måte. 