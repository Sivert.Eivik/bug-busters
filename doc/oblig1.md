# Rapport – innlevering 1
**Team:** *BugBusters* – *Sivert Sundvold Eivik, Eirill Øya, Andreas Lauritzen, Ingeborg Øksnes* 
 
## Oppgave A0

Alle har tidligere erfaring fra inf100, 101 og 102. Med disse fagene har vi fått en innføring i koding, og god kompetanse i objektorientert programmering og algoritmer.

Egenskaper:

Alle medlemmene er gode til å samarbeide og kommunisere. Deltatt mye i samme gruppetimer og hjulpet hverandre med oppgaver gjennom studiet.

Sivert Sundvold Eivik – God kunnskap om datastrukturer og deres tilhørende algoritmer. Er strukturert og har god tilpasningsevne. 

Eirill Øya - Gode problemløsningsevner og er dyktig til å finne effektive løsninger. Glad i den kreative delen av oppgaver, spesielt når det kommer til grafikk.

Ingeborg Øksnes - god evne til å samarbeide og vant med å jobbe i team. Er gruppeleder i inf100, og er god til å lese andres kode, feilsøke og løse problemer.

Andreas Lauritzen - Glad i å samarbeide med andre både ved å kode sammen, men også diskutere hvordan man kan komme frem til gode løsninger. Har laget et spill tidligere i inf101 hvor jeg implementerte ting vi hadde lært og brukt tidligere, men også ved å komme med ideer og kode selv.


## Oppgave A1

Roller:

Teamlead: Sivert Eivik

Frontend: Eirill Haukvik Øya

Testansvarlig: Ingeborg Waagan Øksnes

Backend: Andreas Lauritzen


## Oppgave A2
Vi tenker å lage et spill som ligner super Mario. Vil lage basert på platform -spill.
- Det er en karakter som kan gå til venstre, høyere og hoppe. 
- Det kommer fiender mot karaktern. Disse kan dø ved å bli hoppet på. Hvis karakteren kommer nær fienden blir den skadet
- Spillet er todimensjonalt: 
	- Det er en plattform 
	- Det er hindringer 
- Ved å komme til målet fullfører man banen/ levelen
- Det vil være mulighet for å forbedre karakteren ved Power-ups.
- Utfordringer: fullføre spillet før timeren går ned og unngå fiender 
- Ved å bevege på karakteren vil man se mere av banen. Men du kan ikke returnere til områder du allerede har passert, noe som tvinger deg til å tenke strategisk og ta avgjørelser uten mulighet for å gå tilbake.
- Det er mulighet for å tjene penger. Pengene kan brukes til å forbedre karakteren og få bedre score.

## Oppgave A3
Møter:
Et fast møte i gruppetimen, tirsdag 10:15-12:00. Utenom det så planlegger vi møtene basert på det vi trenger.
Prosjektmetodikk:
Vi ønsker å bruke noen forskjellige elementer fra ulike prosjektmetodikker. Vi ønsker å benytte oss av køsystem med blokkering fra kanban for å begrense antall arbeidsoppgaver på en gang og at oppgaver fullføres før vi begynner på neste steg. Vi ønsker også å bruke elementet med å bryte ned arbeidet i mindre delmål som skal fullføres innen tidsfrister for å sikre god produktivitet i arbeidet. Vi tenker også å benytte oss av parprogrammering, da vi tror det vil føre til mindre feil i koden og raskere fremgang hvis man står fast.

Organisasjon under prosjektet
- Et fast møte i uken på tirsdag fra 10-12. I tillegg til dette avtaler vi et møte til i løpet av uken utifra hva som passer, som også er på 2 timer. Første møtet i uken vil være for å fordele arbeidsoppgaver etc., mens det neste møtet vil være et progresjonsmøte for å se hvordan hvert enkelt medlem ligger an i forhold til fristen som er satt. 
- Vi har en felles discord og felles snapchat-gruppe der vi kommuniserer enkelt med hverandre underveis. Vi sitter også ofte på lesesalen, der det er lav terskel for å diskutere prosjektet.
- Vi har fordelt det slik at alle har et ansvarsområde hver, men alle hjelper hverandre. I starten vil alle samarbeide med å utvikle "startfasen" av spillet, for at det skal bli enklere å bygge på det senere. Selv om man har ulike oppgaver vil vi som regel sitte og jobbe sammen, både får å spørre om hjelp men også for å vise de andre progresjonen for å få alle med på utviklingen. 
- Vi har et møte i uken for å se at alle ligger bra an til fristen som er satt, men det er også lav terskel for å sende melding i Discord/Snapchat gruppe. 
- Vi deler alt av dokumenter og kode, men passer på å ikke pushe/endre ting før alle i gruppen er innforstått med dette for å unngå å overskrive andres arbeid og at alle er enige om endringene som er utført. 

En kort beskrivelse av det overordnede målet for applikasjonen
- Det overordnede målet for applikasjonen vår er å få en spiller fra start til mål

- MVP:
• Vis et spillbrett
• Vis en spiller på skjermen
• Flytte spiller med tastetrykk
• Interaksjon mellom spiller og spillbrett
• Vise motstandere; som interagerer med spillbrett
• Ha hinder/plattformer
• Ha et poengsystem for spilleren
• Spiller taper ved kontakt med motstander eller faller under bakken
• Mål for spillbrettet, spilleren kommer til enden av banen
• Når spillet er over kommer det opp en game over screen, kan komme tilbake til start-skjerm
• Start-skjerm ved oppstart

- Brukerhistorier:
Som bruker trenger jeg å ha muligheten til å vite hvordan spillet fungerer og hvilke knapper jeg skal trykke på for å oppnå forskjellig funksjonalitet. Jeg trenger å tydelig kunne skille mellom spilleren jeg selv styrer, og fiender jeg møter på veien. Det samme gjelder banen mtp hindringer, plattformer og vegger. Jeg må også kunne se hva slags retning jeg skal gå for å komme til mål, uten å måtte bruke unødvendig tid på å finne riktig vei. Hvis spilleren min får oppgraderinger er det viktig at jeg får beskjed om dette ved hjelp av tekst, eller visuelle endringer på karakteren. Hvis det er visse oppgaver som må gjøres underveis i banen er det viktig at dette kommer tydelig fram slik at jeg ikke har behov for å måtte gå tilbake. Når jeg kommer i mål ønsker jeg å få beskjed om hvordan jeg har gjort det slik at jeg kan vite om jeg har gjort det bra, og også om jeg har forbedret meg. 


## Oppgave A4
Har skrevet kode i src

## Oppgave A5
Hva gikk bra?
Alle har vært tilstede på alle møter og gruppetimer. Alle tar like stor del i oppgaven og gjør sin del. Alle er flinke til å lytte til hverandre og komme med kostruktive tilbakemeldinger og svar hvis noen har spørsmål. Så dynamikken i gruppa fungerer veldig bra. Vi har hatt to møter i uken, noe vi tenker har vært lurt. Da har vi på tirsdager fått et overblikk over hva som må gjøres i løpet av uken, og kommet i gang med oblig. Deretter har vi hatt et møte senere i uken for å ferdigstille oblig og høre med hverandre om nye tanker og oppdagelser.
Alle tar like stor del av arbeidet og vi ligger på et veldig likt ferdighetsnivå. Dette gjør også at vi har veldig like felles mål for hvordan vi ønsker at dette prosjektet skal gå.

Hva gikk mindre bra?
Komme mer forberedt til gruppetime for å spare tid. Har vært mye å sette seg inn i og da er det viktig at alle er oppdatert på hvordan vi ligger an og hva som skal gjøres.
Bli flinke til å skrive java docs

Nye aktiviteter eller verktøy som teamet vil prøve ut i løpet av neste obligatoriske oppgave:
Vi ønsker å kode mer sammen, slik at vi kan kode og sette ting sammen med en gang i stedet for å kode hver for oss, se hva de forskjellige har gjort, og deretter måtte sette alt sammen.
Hvordan vi vil sette opp prosjektet med tanke på map, hvor vi vil bruke tiled. Lage spilleren, fiender osv hvor vi tenker å bruke Box2D.
