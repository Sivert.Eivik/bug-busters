# Rapport – innlevering 4
**Team:** BugBusters – Sivert Sundvold Eivik, Andreas Lauritzen, Ingeborg Øksnes, Eirill Øya

# Hvordan fungerer rollene i teamet? Hvordan har det gått med rollene?
Vi synes vi har valgt gode roller som passer for vårt prosjekt. 

# Er det noen erfaringer enten team-messig eller mtp prosjektmetodikk som er verdt å nevne? Synes teamet at de valgene dere har tatt er gode? Hvis ikke, hva kan dere gjøre annerledes for å forbedre måten teamet fungerer på?
Vi har jobbet mye sammen på skolen. Det har derfor vært lett å kommunisere og sammarbeide. Vi har jobbet mere sammen på skolen nå den siste innspurten. Dette er ført til mye godt sammarbeid.

# Hvordan er gruppedynamikken? Er det uenigheter som bør løses?
Vi har hatt god gruppedymanikk hele tiden, og ikke hatt noen uenigheter. 

# Hvordan fungerer kommunikasjonen for dere?
Vi har forsatt hatt god kommuniksajson, og ikke hatt noen problemer med det. 

# Gjør et kort retrospektiv hvor dere vurderer hva dere har klart til nå. Dette skal handle om prosjektstruktur, ikke kode.
Vi har klart å levere et ferdig prosjekt som alle er fornøyd og stolte av. Vi hadde ulike delmål underveis som vi har klar å gjøre og forbedre. Nå sitter vi igjen med et godt ferdig produkt. 

# Oppdater hvilke krav dere har prioritert, hvor langt dere har kommet og hva dere har gjort siden forrige gang.
Vi har jobbet med siste finpuss av spillet, og vi har skrevet tester. Derfor har vi nå på innspurten prioritert å jobbe med tester.


# Har dere gjort justeringer på kravene som er med i MVP? Forklar i så fall hvorfor. Hvis det er gjort endringer i rekkefølge utfra hva som er gitt fra kunde, hvorfor er dette gjort?
Nei, vi har fulgt alle punktene i MVP og forbedret dette som tiden har gått.

# Oppdater hvilke krav dere har prioritert, hvor langt dere har kommet og hva dere har gjort siden forrige gang.
Vi har prioritert tester og forbedring av spillet.

# Husk å skrive hvilke bugs som finnes i de kravene dere har utført (dersom det finnes bugs).
* Vi har slitt med å få spilleren til å ha en stabil fart når den løper og hopper, og når den kommer videre til neste map. 
* Når man skyter to skjeletter med samme fireball, starter animasjonen til det første skjelettet som dør på nytt når det andre blir skutt på.

# Gjør et retrospektiv hvor dere vurderer hvordan hele prosjektet har gått. Hva har dere gjort bra, hva hadde dere gjort annerledes hvis dere begynte på nytt?
Vi synes vi har hatt et veldig godt samarbeid og gruppedynamikk. Kommunikasjonen har gått bra gjennom hele prosjektet. Vi har hatt samme mål. Vi har hatt mange fine møter hvor vi har fått diskutert ting. Vi har hatt en god MVP og et realistisk mål.

Det kunne vært nyttig å skrive tester underveis. Likevel har det vist seg at når vi har gjort dette, endres metodene våre såpass at testene blir ikke lenger nyttige. Derfor kan det være lurt å vente med testing til etter at man er fornøyd og ferdig med en metode. Likevel, det var definitivt noen aspekter av prosjektet som kunne hatt nytte av tidlig testing.

Videre kunne vår bruk av Git vært mer effektiv. Vi har brukt mye tid på at vi har manuelt merget prosjekter. Vi kunne ha spart masse tid dersom vi hadde fortått oss på Git litt tidligere. 




