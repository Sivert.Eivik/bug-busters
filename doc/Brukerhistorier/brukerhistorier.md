## Brukerhistorier for spilleren
Brukerhistorie:
    - Når jeg spiller ønsker jeg at spilleren skal bevege seg på en måte som er logisk og brukervennlig.
Akseptansekriterier:
    - Når brukeren trykker på de ulike knappene skal spilleren bevege seg i samsvar med hva spilleren trykker på. Hvis spilleren trykker på flere knapper samtidig er det den siste knappen som ble trykket på som skal utføres. Det skal kun være mulig å hoppe når spilleren er på bakken, dobbelthopp skal altså ikke være mulig.
Arbeidsoppgaver:
    - Lag en dynamisk spiller i Box2D verden som har en gitt figur, størrelse, masse (vekt), friksjon, hvor alle disse faktorene er med på å lage en spiller som har relativt lik tyngdekraft som her på jorda.
    - Implementer spilleren på en måte som gjør at den kun kan bevege seg på banen som er laget.
    - Lag en metode som har kontroll på når spilleren har lov til å hoppe og ikke. 


Brukerhistorie:
    - Når jeg spiller ønsker jeg at spilleren skal ha riktige animasjoner når jeg utfører bevegelsene som er mulig i spillet. 
Akseptansekriterier:
    - Riktig animasjon skal skje på riktig tidspunkt. Det skal være en smooth overgang mellom animasjoner som gjør at bevegelsene ser realistiske og naturlige ut.
Arbeidsoppgaver:
    - Finne en figur og lage mange bilder med små endringer som til sammen lager en animasjon. Gjør dette for alle formene for animasjoner. Dette innebærer: stå stille, løpe, være i luften, skyte ildkule i luften og på bakken. Samtidig lage dette for når spilleren beveger seg til venstre og når den beveger seg til høyre. Lage player_state som forteller når spilleren gjør de forskjellige bevegelsene/animasjonene. Sette riktig animasjon til riktig bevegelse. 
    

Brukerhistorie:
    - Når jeg spiller ønsker jeg at spilleren ikke dør med mindre det er logisk at jeg dør.
Akseptansekriterier:
    - Spilleren skal kun dø hvis spilleren havner utenfor banen eller hvis den blir truffet av enemy (med mindre den hopper oppå enemy, for da skal enemy dø).
Arbeidsoppgaver:
    - Lage en metode som sjekker om spilleren er innenfor spillbrettet, hvis den er utenfor skal man sette gamestate til game over.
    - Collision detection med enemy objekter, hvis spilleren er på bakken skal den dø og gamestate skal bli game over.
    
## Brukerhistorier score og power-ups
Brukerhistorie:
    - Når jeg spiller ønsker jeg å benytte meg av power ups hvis jeg har gjort meg fortjent til det. Jeg ønsker også at power ups skal gi meg en fordel i spillet.
Akseptansekriterier:
    - Spilleren skal kunne ha mulighet til å bruke poeng/mynter til å skyte ildkuler. Disse ildkulene skal kunne drepe enemies hvis ildkulen treffer enemy. Hvis ildkulen bommer skal den ikke påvirke spillet på noen måte. Spilleren skal selv kunne velge når den vil benytte seg av ildkuler. Det skal også være lett å se om man har lyktes med å treffe eller ikke. Hvis spilleren treffer en enemy, så skal enemy tydelig dø.
Arbeidsoppgaver:
    - Lag en ildkule som har en retning høyre eller venstre i forhold til hvilken vei spilleren ser. Ildkulen skal ha en fast bane rett frem uten å bli påvirket av gravitsjon. Lag en collision detection for å sjekke om ildkulen treffer noe. Hvis den treffer en enemy, fjern enemy og ildkulen. Hvis den treffer banen eller går utenfor banen, fjern ildkulen. Legg til animasjon på ildkulen så det er lett å se at du har skutt en ildkule.

Brukerhistorie:
    - Når jeg spiller ønsker jeg å opparbeide meg penger som gjør at jeg kan benytte seg av ildkuler i spillet. 
Akseptansekriterier:
    - Det skal være lett å se at blokkene med penger er noe man ønsker å få tak i. Det skal gis en viss pengesum når man får tak i disse blokkene. Disse pengene skal være mulig å bruke, og ikke bare et tall på skjermen, slik at spilleren får fordeler av å samle de. Pengeblokkene som blir plukket opp skal forsvinne ved kontakt som gjør at spilleren forstår at den er plukket opp. Hvor mye pengerspilleren har skal også være synlig på skjermen. 
Arbeidsoppgaver:
Legg til blokker i banen som spilleren kan gå gjennom. Implementer collision detection for å se om spilleren treffer en pengeblokk. Hvis den gjør det, fjern denne blokken og legg til en viss pengesum. Hvis spilleren har nok penger skal det være mulig å bruke de på ildkuler. Fjern deretter penger i fohold til hvor mye penger som er brukt. 

## Brukerhistorier for Playbutton
Brukerhistorie:  
    - Når jeg spiller ønsker jeg å kunne starte spillet ved å trykke på "Play" knappen.

Akseptansekriterier:    
    - Når brukeren trykker på "Play" knappen, skal spillet starte og gå til hovedspillskjermen.
    - Det skal ikke være mulig å starte spillet hvis det allerede pågår.

Arbeidsoppgaver:  
    - Implementer funksjonalitet for å starte spillet når "Play" knappen trykkes.
    - Sørg for at spillet ikke kan startes hvis det allerede er i gang.

## Brukerhistorier for ControlsButton
Brukerhistorie:  
    - Som spiller ønsker jeg å kunne se kontrollene og spillinformasjonen ved å trykke på "Controls" knappen.

Akseptansekriterier:    
    - Når brukeren trykker på "Controls" knappen, skal spillet vise informasjonssiden med kontroller og annen spillinformasjon.
    - Det skal være mulig å navigere tilbake til hovedmenyen fra informasjonssiden.

Arbeidsoppgaver:  
    - Implementer funksjonalitet for å vise kontroller og spillinformasjon når "Controls" knappen trykkes.
    - Designe og lage selve controls siden
    - Legg til en tilbake-knapp eller funksjon for å gå tilbake til hovedmenyen fra informasjonssiden.

## Brukerhistorier for ExitButton
Brukerhistorie:  
    - Som spiller ønsker jeg å kunne avslutte spillet ved å trykke på "Exit" knappen.

Akseptansekriterier:    
    - Når brukeren trykker på "Exit" knappen, skal spillet avsluttes og lukkes.

Arbeidsoppgaver:  
    - Implementer funksjonalitet for å avslutte spillet når "Exit" knappen trykkes.

## Brukerhistorier for MenuScreen
Brukerhistorie:  
    - Som spiller ønsker jeg å kunne navigere og samhandle med menyen for å kunne starte spillet, se spillkontroller og avslutte spillet.

Akseptansekriterier:     
    - Menyskjermen skal vises når spillet starter.
    - Knappene skal endre utseende når brukeren holder musen over dem.
    - Det skal være visuell tilbakemelding når knappene trykkes ned.

Arbeidsoppgaver:  
    - Design og opprett en menybakgrunn som vises når menyen åpnes.
    - Lag tre knapper for "Play", "Controls" og "Exit" og legg til tekstur og størrelse.
    - Implementer funksjonalitet for å endre gamestate når knappene trykkes.
    - Legg til effekter for å endre knappeutseende når musen holdes over dem.
    - Legg til visuell tilbakemelding når knappene trykkes ned.

## Brukerhistorier for PauseButton
Brukerhistorie:  
    - Som spiller ønsker jeg å kunne sette spillet på pause ved å trykke på en dedikert knapp mens jeg spiller.

Akseptansekriterier:    
    - Knappen skal vises i et hjørne av skjermen når spillet er i "Playing" modus.
    - Når brukeren trykker på knappen mens spillet spilles, skal spillet settes på pause.
    - Knappens utseende skal endres når musen dras over den.
    - Visuell tilbakemelding skal gis når knappen trykkes ned.

Arbeidsoppgaver:  
    - Design og opprett en pauseknapp for å plassere den i et hjørne av skjermen.
    - Implementer funksjonalitet for å sette spillet på pause når knappen trykkes.
    - Legg til effekter for å endre knappeutseende når musen holdes over den.
    - Legg til visuell tilbakemelding når knappen trykkes ned.


## Brukerhistorier for PauseMeny
Brukerhistorie:  
    - Som spiller ønsker jeg å kunne se og interagere med en pausemeny når spillet er satt på pause, slik at jeg kan fortsette, gå tilbake til hovedmenyen eller avslutte spillet.
 
Akseptansekriterier:    
    - Når spillet er satt på pause, skal en pausemeny vises på skjermen.
    - Pausemenyen skal inneholde tre knapper: "Play/Continue", "Home" og "Exit".
    - Knappene skal kunne bli trykket på for å utføre tilhørende handlinger.
    - Knappenes utseende skal endres når musen dras over dem.
    - Visuell tilbakemelding skal gis når knappene trykkes ned.
 
Arbeidsoppgaver:  
    - Design og opprett en pausemeny med tre knapper.
    - Implementer funksjonalitet for hver knapp for å utføre tilhørende handlinger (fortsette spillet, gå   tilbake til hovedmenyen, avslutte spillet).
    - Legg til effekter for å endre knappeutseende når musen holdes over dem.
    - Legg til visuell tilbakemelding når knappene trykkes ned.

## Brukerhistorie for TiledMap
Brukerhistorie:
    - Når jeg lager kartet, vil jeg at kartet skal ha ulike objekter som interagerer med spilleren forskjellig.
    - Fargene på kartet må ta hensyn til personer med nedsatt fargesyn.
    - Kartet skal gi mening, med tanke på fremgang i spillet 
    - Kartet skal lastes slik at vinduet kun viser en del av kartet.
Akseptansekriterier:
    - Når spilleren står på bakken, skal spilleren kunne bevege seg
    - Når spilleren ikke står på bakken, vil spilleren falle nedover
    - Fargene på kartet må ikke være for like. For eksempel må bakken ha en forskjellig farge sammenliknet med bakgrunnen.
Arbeidsoppgaver:
    - Lag et kart der bakken er et objekt som spilleren kan stå og gå på. 
    - Prøv ulike kombinasjoner av bakgrunnsfarge, farge på bakke osv., for å finne en kombinasjon som tar hensyn til nedsatt fargesyn.
    - Rendre kartet riktig slik at ikke hele kartet vises med en gang, men kun en del.

Brukerhistorie:
    - Når jeg spiller ønsker jeg at kartet skal være enkelt å forstå.
    - Når jeg spiller ønsker jeg at kartet skal fungere på en logisk måte uten bugs.
    - Når jeg spiller ønsker jeg at fargene på kartet ikke skal være for like.
Akseptansekriterier:
    - Oppbygningen av kartet gir mening og jeg skjønner hvordan jeg skal spille spillet for fremgang.
    - Jeg skal ikke kunne bli "stuck" i kartet grunent bugs eller at kartet ikke vises.
    - Det er tydelige forskjeller slik at jeg ikke misforstår hva som er hva i kartet mtp. farge.
Arbeidsoppgaver:
    - Lage map der det er tydelig hvordan fremgang skal oppnås.
    - Rendere map og lage map i Tiled på en måte slik at bugs ikke oppstår. Dette inngår å dele ulike deler i kartet inn i 
ulike typer og objekter for å oppnå riktig logikk.
    - Sørge for at fargene som brukes for forskjellige objekter ikke er for like. 

## Brukerhistorie for lydeffekter
Brukerhistorie:
    - Når jeg legger til lydeffekter, ønsker jeg lydeffekter som gir mening med tanke på GameState.
    - Lydene må ikke være for høye eller slitsomme å høre på i lengden. 
    - Lydene må spilles av på riktig tid.
Akseptansekriterier:
    - Godt valg av lyd og musikk med tanke på spillets situasjoner.
    - Lydene er tilpasset med tanke på volum og lydene/musikken som er valgt er ikke slitsomme.
    - Lydene spilles av på riktig tidspunkt.
Arbeidsoppgaver:
    - For å spille av riktig lyd gitt situasjonen i spillet må jeg bruke GameStates og Controller for at lyden skal spilles av riktig.
    - Sørge for at lyden ikke er for høy eller for lav, men justert riktig. 
    - Velge musikk/lyd som jeg tenker spilleren vil like.

Brukerhistorie for fiender (spiller):
    • Som spiller, ønsker jeg at fiendene skal ha et karakteristisk ondt utseende, slik at jeg enkelt kan identifisere dem som en trussel og forstå at jeg skal unngå eller bekjempe dem.
Akseptansekriterier:
    • Fiendens design skal være unikt og umiddelbart gjenkjennelig sammenlignet med andre karakterer og omgivelser.
    • Spilltesterne skal kunne identifisere fienden som en trussel uten forutgående forklaring.
    • Spillet skal inneholde en introduksjon eller et opplæringsnivå hvor spilleren lærer å gjenkjenne og reagere på fienden.
Arbeidsoppgaver:
    • Design og utvikle flere unike fiende-karaktermodeller med et ondt utseende.
    • Implementere logikk for at fiender umiddelbart blir gjenkjent som en trussel av spilleren.


## Brukerhistorie for fiender (utvikler).
    • Som utvikler, ønsker jeg å sikre at alle interaksjoner med fienden er grundig testet, slik at spillerne får en feilfri og engasjerende opplevelse når de møter fiender i spillet.
Akseptansekriterier:
    • Sørge for at fiendens angrepsmønstre fungerer som forventet under ulike spillforhold.
    • Bekrefte at spillerens angrep påvirker fienden på de tiltenkte måtene.
Arbeidsoppgaver:
    • implementere og teste fiendens angrepsmønstre i forskjellige spillforhold.
    • Sikre at spillerens handlinger har de riktige effektene på fiendene.
    • Gjennomføre omfattende tester for å sikre at interaksjonene med fienden er uten feil og engasjerende.
    • Optimalisere fiendens adferd for å oppnå en balansert spillopplevelse.

## Brukerhistorie for tester.
    • Som utvikler, ønsker jeg å vite at metodene jeg har skrevet er korrekte. Jeg ønsker å bekrefte at de er kodet korrekt. Samtidig er det en garanti for de andre på gruppeprosjektet om at alt fungerer slikt som det skal. Det er viktig å teste funksjonaliteten til koden, så vi vet at koden fungerer som den skal. 
Akseptansekriterier:
    • Kodegjennomgang: Hver metode skal gjennomgås av minst ett annet teammedlem for å sikre at koden er klar, konsis og følger prosjektets kodekonvensjoner.
    • Enhetstesting: Alle metoder skal ha tilhørende enhetstester som dekker alle mulige brukstilfeller og kanttilfeller for metoden.
    • Integrasjonstesting: Metodene skal testes for å sikre at de integreres korrekt med resten av systemet og samhandler riktig med andre komponenter.
    • Kodekjøringsresultater: Koden skal kjøre uten feil eller uventet atferd i den tiltenkte brukskonteksten.
    • Ytelsesvalidering: Metodene skal oppfylle de forventede ytelseskriteriene uten å forårsake unødvendig belastning eller forsinkelser i systemet.
    • Brukertesting: Funksjonaliteten som metodene støtter skal testes av brukere for å bekrefte at de oppfører seg som forventet fra mottakers perspektiv.
Arbeidsoppgaver:
    • Koordinere en kodegjennomgang for hver ny metode med minst ett annet teammedlem.
    • Utvikle og kjøre enhetstester for alle nye metoder, inkludert kanttilfeller.
    • Utføre integrasjonstesting for å bekrefte korrekt samhandling mellom nye metoder og eksisterende systemkomponenter.
    • Utføre ytelsesvalidering for å sikre at metodene møter forventede ytelseskrav uten å forårsake systembelastning.
Organisere brukertester for å bekrefte at metodene fungerer som forventet fra sluttbrukernes perspektiv.