# INF112 libGDX + Maven template 
Simple skeleton with [libGDX](https://libgdx.com/). 

# INF112 Project – *Spill-navn*

* Team: *BugBusters* (Gruppe 5): *Ingeborg Waagan Øksnes, Eirill Haukvik Øya, Andreas Lauritzen, Sivert Eivik*
* https://git.app.uib.no/Sivert.Eivik/bug-busters

## Om spillet
*« Et plattformspill inspirert av Super Mario, hvor spilleren styrer en figur som kan bevege seg til venstre, høyre og hoppe for å overvinne hindringer og fiender. I dette todimensjonale eventyret må figuren nå målet for å fullføre nivået, samtidig som den samler power-ups og penger for å forbedre egenskaper og score. Spillet legger vekt på strategisk fremdrift uten returmuligheter, og med utfordringer som tidsbegrensning og fiendeunngåelse.»*

## Kjøring
* For å kjøre spillet må du kjøre følgende kommandoer i terminalen:
* mvn compile
* mvn exec:java

## Kjente feil


## Credits
https://en.wikipedia.org/wiki/Mario_Bros.


# Maven Setup
This project comes with a working Maven `pom.xml` file. You should be able to import it into Eclipse using *File → Import → Maven → Existing Maven Projects* (or *Check out Maven Projects from SCM* to do Git cloning as well). You can also build the project from the command line with `mvn clean compile` and test it with `mvn clean test`.

Pay attention to these folders:
* `src/main/java` – Java source files go here (as usual for Maven) – **IMPORTANT!!** only `.java` files, no data files / assets
* `src/main/resources` – data files go here, for example in an `assets` sub-folder – **IMPORTANT!** put data files here, or they won't get included in the jar file
* `src/test/java` – JUnit tests
* `target/classes` – compiled Java class files

**TODO:** You should probably edit the `pom.xml` and fill in details such as the project `name` and `artifactId`:


```xml

	< !-- FIXME - set group id -->
	<groupId>inf112.skeleton.app</groupId>
	< !-- FIXME - set artifact name -->
	<artifactId>gdx-app</artifactId>
	<version>1.0-SNAPSHOT</version>
	<packaging>jar</packaging>

	< !-- FIXME - set app name -->
	<name>mvn-app</name>
	< !-- FIXME change it to the project's website -->
	<url>http://www.example.com</url>
```

	
## Running
You can run the project with Maven using `mvn exec:java`. Change the main class by modifying the `main.class` setting in `pom.xml`:

```
		<main.class>inf112.skeleton.app.Main</main.class>
```

Running the program should open a window with the text “Hello, world!” and an alligator in the lower left corner.  Clicking inside the window should play a *blip* sound. Exit by pressing *Escape* or closing the window.

You may have to compile first, with `mvn compile` – or in a single step, `mvn compile exec:java`.

## Testing
Run unit tests with `mvn test` – unit test files should have `Test` in the file name, e.g., `ExampleTest.java`. This will also generate a [JaCoCo](https://www.jacoco.org/jacoco) code coverage report, which you can find in [target/site/jacoco/index.html](target/site/jacoco/index.html).

Use `mvn verify` to run integration tests, if you have any. This will do everything up to and including `mvn package`, and then run all the tests with `IT` in the name, e.g., `ExampleIT.java`.

## Jar Files

If you run `mvn package` you get everything bundled up into a `.jar` file + a ‘fat’ Jar file where all the necessary dependencies have been added:

* `target/NAME-VERSION.jar` – your compiled project, packaged in a JAR file
* `target/NAME-VERSION-fat.jar` – your JAR file packaged with dependencies

Run Jar files with, for example, `java -jar target/NAME-VERSION-fat.jar`.


If you have test failures, and *really* need to build a jar anyway, you can skip testing with `mvn -Dmaven.test.skip=true package`.

## Git Setup
If you look at *Settings → Repository* in GitLab, you can protect branches – for example, forbid pushing to the `main` branch so everyone have to use merge requests.


# Credits

### Template files
* `src/main/resources/pauseKnapper.png` – 
game PNG Designed By CONG from https://pngtree.com/freepng/game-pause-button-game-switch_5321002.html?sol=downref&id=bef

gui PNG Designed By Johnstocker from https://pngtree.com/freepng/cute-square-game-button-set-interface-gui_7257018.html?sol=downref&id=bef

* `src/main/resources/controlsScreen.png (buttons)`– 
laptop PNG Designed By VectorDes1gn from https://pngtree.com/freepng/laptop-and-computer-keyboards-vector-design_6236574.html?sol=downref&id=bef

office PNG Designed By VectorDes1gn from https://pngtree.com/freepng/laptop-and-computer-keyboards-2-vector-design_6236575.html?sol=downref&id=bef

* `src/main/resources/buttons.png`– 
gui PNG Designed By Johnstocker from https://pngtree.com/freepng/fantasy-design-for-game-buttons-set_7257251.html?sol=downref&id=bef

* `src/main/resources/pauseBoard.png`– 
game PNG Designed By Thi Minh from https://pngtree.com/freepng/popup-wooden-game_6839509.html?sol=downref&id=bef

* `src/main/resources/animationAtlas.png`– gbtemplate by NBJDLukasAbsolute https://opengameart.org/content/gb-style-platformer-character-template

* `src/main/resources/background.mp3.png`– Cruising down 8bit Lane by Monument music from https://pixabay.com/music/search/platformer/

* `src/main/resources/victory.mp3`– GoodResult by Pixabay from https://pixabay.com/sound-effects/goodresult-82807/ 

* `src/main/resources/gameover.mp3`– negative_beeps by Pixabay from https://pixabay.com/sound-effects/negative-beeps-6008/

* `src/main/assets/tiles.png`– "PlataGO! Super Platform Game Maker" by Patashnik from https://www.spriters-resource.com/pc_computer/platagosuperplatformgamemaker/sheet/113240/

* `src/main/assets/death_and_walk_animation.png`– animated skeleton by Calciumtrice https://opengameart.org/content/animated-skeleton

