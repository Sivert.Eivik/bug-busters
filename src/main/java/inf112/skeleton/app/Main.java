package inf112.skeleton.app;

import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Application;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3ApplicationConfiguration;

import inf112.skeleton.app.model.MarioModel;
import inf112.skeleton.app.view.GameView;

public class Main {
    public static void main(String[] args) {
        Lwjgl3ApplicationConfiguration cfg = new Lwjgl3ApplicationConfiguration();
        cfg.setTitle("Game");
        cfg.setWindowedMode(640, 320);
        MarioModel model = new MarioModel();
        GameView view = new GameView(model);

        new Lwjgl3Application(view, cfg);
    }
}
