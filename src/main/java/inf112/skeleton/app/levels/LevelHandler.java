package inf112.skeleton.app.levels;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;

import inf112.skeleton.app.controller.PlayerController;
import inf112.skeleton.app.map.BodyCreation;
import inf112.skeleton.app.map.MapCollision;
import inf112.skeleton.app.model.*;

public class LevelHandler extends BodyCreation {
    private TiledMap map;
    private BodyCreation bodyCreation;
    private World world;
    private Box2DPlayer box2DPlayer;
    private OrthogonalTiledMapRenderer mapRenderer;
    private MapCollision mapCollision;
    private Array<Body> enemies;
    private Enemy enemy;
    private Fireball fireball;
    private SpriteBatch batch;
    private OrthographicCamera camera;
    private Player player;
    private PlayerController playerController;
    private BitmapFont font;
    private GlyphLayout layout;
    private PauseButtons pauseButtons;
    private int level = 1;
    private static final int VIEWPORT_BLOCKS_WIDTH = 20;
    private static final int SCREENWIDTH = 640;

    public LevelHandler(World world, Box2DPlayer box2DPlayer, Enemy enemy, Player player, BodyCreation bodyCreation) {
        this.world = world;
        this.box2DPlayer = box2DPlayer;
        this.enemy = enemy;
        this.player = player;
        this.bodyCreation = new BodyCreation();
        camera = new OrthographicCamera();
        camera.setToOrtho(false, 620, 320);
    }

    public LevelHandler(World world, Box2DPlayer box2DPlayer, Enemy enemy,
            SpriteBatch batch, Player player, PlayerController playerController, MapCollision mapCollision,
            PauseButtons pauseButtons) {
        this.world = world;
        this.box2DPlayer = box2DPlayer;
        mapRenderer = new OrthogonalTiledMapRenderer(map);
        this.mapCollision = mapCollision;
        this.enemy = enemy;
        this.batch = batch;
        camera = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        camera.setToOrtho(false, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        this.player = player;
        this.playerController = playerController;
        font = new BitmapFont();
        layout = new GlyphLayout();
        this.pauseButtons = pauseButtons;
        bodyCreation = new BodyCreation();
    }

    /**
     * Loads the current map based on level number
     * 
     * @param levelNumber - current level number
     */
    public void loadLevel(int levelNumber) {
        Level1 level1 = new Level1();
        Level2 level2 = new Level2();
        Level3 level3 = new Level3();
        switch (levelNumber) {
            case 1:
                this.map = level1.map();
                box2DPlayer.setPosition();
                destroyBodies();
                createBodies();
                break;
            case 2:
                level++;
                this.map = level2.map();
                box2DPlayer.setPosition();
                destroyBodies();
                createBodies();
                break;
            case 3:
                level++;
                this.map = level3.map();
                box2DPlayer.setPosition();
                destroyBodies();
                createBodies();
                break;
        }
    }

    /**
     * Renders the game scene, including the map, enemy, player etc.
     * This method is responsible for rendering a new map after earlier map is
     * complete.
     */
    public void render() {
        batch.setProjectionMatrix(camera.combined);
        batch.begin();
        if (playerController.getFireball() != null) {
            fireball = playerController.getFireball();
            fireball.renderFireball(fireball, batch);
            fireball.moveFireball(box2DPlayer);
            float fireballPosX = fireball.getFireBall().getPosition().x;
            if (fireballPosX / 32 >= camera.position.x / 32 + VIEWPORT_BLOCKS_WIDTH / 2) {
                fireball.removeFireball(fireball.getFireBall());
            } else if (fireballPosX / 32 <= 0) {
                fireball.removeFireball(fireball.getFireBall());
            }
        }
        mapCollision.updateFinishCollision(map);
        mapRenderer.setMap(map);
        mapRenderer.setView(camera);
        mapRenderer.render();
        enemy.render(enemies, batch);
        enemy.renderDeathAnimation(batch);
        player.renderPlayerAnimations(batch, box2DPlayer.getBody());
        playerController.fromAirToStandingStill();
        enemy.updateEnemyArray(enemies);
        playerOutSideViewPort();
        layout = new GlyphLayout(font, "LEVEL: " + level + "/3");
        font.setColor(Color.BLACK);
        float x = camera.position.x - layout.width / 2;
        float y = camera.position.y + camera.viewportHeight / 2 - layout.height;
        font.draw(batch, layout, x, y);
        pauseButtons.drawPauseButton(batch, camera);
        pauseButtons.drawPauseButton(batch, camera);
        batch.end();
        camera.update();
        world.step(Gdx.graphics.getDeltaTime(), 6, 2);
    }

    /**
     * Adjusts the camera position based on the player's movement and the width of
     * the map,
     * ensuring that the player remains within the visible viewport.
     */
    private void playerOutSideViewPort() {
        int mapWidth = map.getProperties().get("width", Integer.class);
        Vector3 playerPosition = new Vector3(box2DPlayer.getBody().getPosition(), 0);
        if (playerPosition.x / 32 >= mapWidth - VIEWPORT_BLOCKS_WIDTH / 2) {
            camera.position.x = mapWidth * 32 - VIEWPORT_BLOCKS_WIDTH / 2 * 32;
        } else if (playerPosition.x / 32 >= VIEWPORT_BLOCKS_WIDTH / 2) {
            camera.position.x = playerPosition.x;
        } else {
            camera.position.x = SCREENWIDTH / 2;
        }
    }

    /**
     * Method to return the current tiled map
     * 
     * @return current Tiled map
     */
    public TiledMap currentMap() {
        return map;
    }

    /**
     * Method to return the current position for the camera
     * 
     * @return current camera position
     */
    public Vector3 currentCameraPosition() {
        return camera.position;
    }

    /**
     * Destroys all bodies except player when a new map is rendered, to prevent
     * earlier entities like enemies etc. being a part of the new map.
     */
    private void destroyBodies() {
        Array<Body> bodies = new Array<Body>();
        for (Body body : world.getBodies(bodies)) {
            if (body != box2DPlayer.getBody()) {
                world.destroyBody(body);
            }
        }
    }

    /**
     * Creates all the bodies for the new map loaded, except for the body of the
     * player.
     *
     */
    private void createBodies() {
        bodyCreation.createBody(map, world, "enemies", BodyType.DynamicBody, "enemies");
        bodyCreation.createBody(map, world, "obstacle", BodyType.StaticBody, "obstacle");
        bodyCreation.createBody(map, world, "ground", BodyType.StaticBody, "ground");
        enemies = enemy.enemyArray(map, world, "enemies", BodyType.DynamicBody, "enemies");
    }
}
