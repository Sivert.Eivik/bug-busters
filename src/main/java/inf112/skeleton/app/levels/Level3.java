package inf112.skeleton.app.levels;

import com.badlogic.gdx.maps.tiled.TiledMap;
import inf112.skeleton.app.map.MapHandler;

public class Level3 {
    public TiledMap map() {
        MapHandler mapHandler = new MapHandler();
        String stringPath = "src/main/java/inf112/skeleton/app/assets/level3.tmx";
        TiledMap tiledMap = mapHandler.loadMap(stringPath);
        return tiledMap;
    }
}
