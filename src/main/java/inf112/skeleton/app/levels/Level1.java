package inf112.skeleton.app.levels;

import com.badlogic.gdx.maps.tiled.TiledMap;

import inf112.skeleton.app.map.MapHandler;

public class Level1 {
    /**
     * Method to return Tiled map for level 1
     * 
     * @return Level 1 Tiled map
     */
    public TiledMap map() {
        MapHandler mapHandler = new MapHandler();
        String stringPath = "src/main/java/inf112/skeleton/app/assets/level1.tmx";
        TiledMap tiledMap = mapHandler.loadMap(stringPath);
        return tiledMap;
    }
}
