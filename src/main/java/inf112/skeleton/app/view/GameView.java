package inf112.skeleton.app.view;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.ScreenUtils;

import inf112.skeleton.app.controller.PlayerController;
import inf112.skeleton.app.levels.LevelHandler;
import inf112.skeleton.app.map.MapCollision;
import inf112.skeleton.app.model.*;

public class GameView implements ApplicationListener {
    private World world;
    private Box2DPlayer Box2DPlayer;
    private Player player;

    private SpriteBatch batch;
    private Texture winnerTexture;
    private Texture gameOverTexture;

    private MapCollision mapCollision;

    private GameState gameState;
    private MarioModel model;
    private Enemy enemy;

    private PlayerController playerController;
    private MenuScreen menuScreen;
    private PauseButtons pauseButtons;
    private LevelHandler levelHandler;
    private int levelNumber = 1;
    private static final int NUMBER_OF_MAPS = 3;
    private PlayerState playerState;
    private GameSounds gameSounds;
    private Fireball fireball;

    public GameView(MarioModel model) {
        this.model = model;
        this.gameState = GameState.WELCOME;
    }

    @SuppressWarnings("deprecation")
    @Override
    public void create() {
        this.playerState = PlayerState.STANDING_RIGHT;
        menuScreen = new MenuScreen(new SpriteBatch());
        enemy = new Enemy(model, this);
        world = new World(new Vector2(0, -80), true);
        batch = new SpriteBatch();
        player = new Player(playerState);
        pauseButtons = new PauseButtons(menuScreen);
        mapCollision = new MapCollision();
        Box2DPlayer = new Box2DPlayer(world);
        gameSounds = new GameSounds(this);
        playerController = new PlayerController(Box2DPlayer.getBody(), player, this, menuScreen, world, fireball, enemy,
                pauseButtons);
        world.setContactListener(playerController);
        levelHandler = new LevelHandler(world, Box2DPlayer, enemy, batch, player, playerController, mapCollision,
                pauseButtons);
        levelHandler.loadLevel(levelNumber);
        winnerTexture = new Texture(Gdx.files.internal("victory.png"));
        gameOverTexture = new Texture(Gdx.files.internal("gameover.png"));
        Gdx.input.setInputProcessor(playerController);
        Gdx.graphics.setForegroundFPS(60);
    }

    /**
     * Sets the gamestate
     * 
     * @param gameState - GameState
     */
    public void setGameState(GameState gameState) {
        this.gameState = gameState;
    }

    /**
     * Returns the current gamestate
     * 
     * @return current gamestate
     */
    public GameState getGameState() {
        return gameState;
    }

    @Override
    public void resize(int width, int height) {
        batch.setProjectionMatrix(batch.getProjectionMatrix());
    }

    @Override
    public void render() {
        gameSounds.playMusic();
        if (gameState == GameState.WELCOME) {
            batch.begin();
            menuScreen.drawWelcomeScreen(batch, levelHandler);
            batch.end();
        }
        if (gameState == GameState.EXIT) {
            Gdx.app.exit();
        }
        if (gameState == GameState.CONTROLS) {
            batch.begin();
            menuScreen.drawControlsScreen(batch, levelHandler);
            batch.end();
        }
        if (gameState == GameState.PAUSED) {
            batch.begin();
            enemy.enemyMovement();
            pauseButtons.drawPauseMenu(batch, levelHandler.currentCameraPosition());
            batch.end();
        }
        if (gameState == GameState.PLAYING) {
            ScreenUtils.clear(Color.WHITE);
            enemy.enemyMovement();
            world.step(Gdx.graphics.getDeltaTime(), 6, 2);
            levelHandler.render();
            if (mapCollision.finishCollision(Box2DPlayer, levelHandler.currentMap())) {
                if (levelNumber <= NUMBER_OF_MAPS) {
                    levelNumber++;
                    levelHandler.loadLevel(levelNumber);
                } else {
                    gameState = GameState.WINNER;
                }
            }
            if (!Box2DPlayer.insideWorld(world, levelHandler.currentMap())) {
                gameState = GameState.GAME_OVER;
            }
        }
        if (gameState == GameState.WINNER) {
            winnerTexture = new Texture(Gdx.files.internal("victory.png"));
            ScreenUtils.clear(Color.WHITE);
            batch.begin();
            batch.getProjectionMatrix().setToOrtho2D(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
            batch.draw(winnerTexture, 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
            batch.end();
        }
        if (gameState == GameState.GAME_OVER) {
            ScreenUtils.clear(Color.WHITE);
            batch.begin();
            batch.getProjectionMatrix().setToOrtho2D(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
            batch.draw(gameOverTexture, 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
            batch.end();
        }
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

    @Override
    public void dispose() {
        batch.dispose();
        gameOverTexture.dispose();
        winnerTexture.dispose();
    }
}
