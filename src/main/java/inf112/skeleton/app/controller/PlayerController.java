package inf112.skeleton.app.controller;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.badlogic.gdx.physics.box2d.World;

import inf112.skeleton.app.model.*;
import inf112.skeleton.app.view.GameView;

public class PlayerController implements InputProcessor, ContactListener {
    private Body body;
    private boolean movingRight = false;
    private boolean movingLeft = false;
    public boolean ableToJump = false;
    private GameView gameView;
    private int groundContacts = 0;
    private Player player;
    private Fireball fireball;
    private World world;
    private Enemy enemy;
    private PauseButtons pauseButtons;

    private MenuScreen menuScreen;

    /**
     * Constructor used for testing
     * 
     * @param body
     * @param player
     * @param gameView
     */
    public PlayerController(Body body, Player player, GameView gameView, World world) {
        this.body = body;
        this.player = player;
        this.gameView = gameView;
        this.world = world;
    }

    public PlayerController(Body body, Player player, GameView gameView, MenuScreen menuScreen, World world,
            Fireball fireball, Enemy enemy, PauseButtons pauseButtons) {
        this(body, player, gameView, world);
        this.menuScreen = menuScreen;
        this.world = world;
        this.fireball = fireball;
        this.enemy = enemy;
        this.pauseButtons = pauseButtons;

    }

    @Override
    public boolean keyDown(int keycode) {
        if (gameView.getGameState() == GameState.CONTROLS) {
            if (keycode == Input.Keys.ESCAPE) {
                gameView.setGameState(GameState.WELCOME);
            }
        }
        if (keycode == Input.Keys.D) {
            movingRight = true;
            moveRight(movingRight);
            if (ableToJump) {
                player.setPlayerState(PlayerState.RUNNING_RIGHT);
            } else {
                player.setPlayerState(PlayerState.JUMPING_RIGHT);
            }
        }
        if (keycode == Input.Keys.A) {
            movingLeft = true;
            moveLeft(movingLeft);
            if (ableToJump) {
                player.setPlayerState(PlayerState.RUNNING_LEFT);
            } else {
                player.setPlayerState(PlayerState.JUMPING_LEFT);
            }
        }
        if (keycode == Input.Keys.SPACE && ableToJump) {
            jump();
            if (body.getLinearVelocity().x > 0) {
                player.setPlayerState(PlayerState.JUMPING_RIGHT);
            } else if (body.getLinearVelocity().x < 0) {
                player.setPlayerState(PlayerState.JUMPING_LEFT);
            }
            if (player.getPlayerState() == PlayerState.STANDING_LEFT) {
                player.setPlayerState(PlayerState.JUMPING_LEFT);
            } else {
                player.setPlayerState(PlayerState.JUMPING_RIGHT);
            }

        }
        if (keycode == Input.Keys.SHIFT_LEFT && fireball.fireBallArray.isEmpty()) {
            fireball = new Fireball(world, body, player);
            fireball.addFireBall(fireball.getFireBall());
            if (player.getPlayerState() == PlayerState.STANDING_RIGHT ||
                    player.getPlayerState() == PlayerState.RUNNING_RIGHT) {
                player.setPlayerState(PlayerState.SHOOTING_RIGHT);
            } else if (player.getPlayerState() == PlayerState.STANDING_LEFT ||
                    player.getPlayerState() == PlayerState.RUNNING_LEFT) {
                player.setPlayerState(PlayerState.SHOOTING_LEFT);
            } else if (player.getPlayerState() == PlayerState.JUMPING_RIGHT) {
                player.setPlayerState(PlayerState.SHOOTING_MID_AIR_RIGHT);
            } else if (player.getPlayerState() == PlayerState.JUMPING_LEFT) {
                player.setPlayerState(PlayerState.SHOOTING_MID_AIR_LEFT);
            }
        }
        return false;
    }

    private void jump() {
        if (movingRight) {
            body.applyLinearImpulse(new Vector2(body.getMass() * 100, body.getMass() * 200), body.getWorldCenter(),
                    true);
        } else if (movingLeft) {
            body.applyLinearImpulse(new Vector2(-body.getMass() * 100, body.getMass() * 200), body.getWorldCenter(),
                    true);
        } else {
            body.applyLinearImpulse(new Vector2(body.getLinearVelocity().x, body.getMass() * 100),
                    body.getWorldCenter(), true);
        }
    }

    @Override
    public boolean keyUp(int keycode) {
        if (keycode == Input.Keys.D) {
            movingRight = false;
            moveRight(movingRight);
        }
        if (keycode == Input.Keys.A) {
            movingLeft = false;
            moveLeft(movingLeft);
        }
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        if (gameView.getGameState() == GameState.WELCOME) {
            if (menuScreen.isButtonPressed(menuScreen.playButtonRectangle)) {
                gameView.setGameState(GameState.PLAYING);
            } else if (menuScreen.isButtonPressed(menuScreen.exitButtonRectangle)) {
                Gdx.app.exit();
            } else if (menuScreen.isButtonPressed(menuScreen.controlButtonRectangle)) {
                gameView.setGameState(GameState.CONTROLS);
            }
        }
        if (gameView.getGameState() == GameState.PLAYING) {
            if (menuScreen.isButtonPressed(pauseButtons.pauseButtonRectangle)) {
                gameView.setGameState(GameState.PAUSED);
            }
        }
        if (gameView.getGameState() == GameState.PAUSED) {
            if (menuScreen.isButtonPressed(pauseButtons.continueButtonRectangle)) {
                gameView.setGameState(GameState.PLAYING);
            } else if (menuScreen.isButtonPressed(pauseButtons.exitButtonRectangle)) {
                Gdx.app.exit();
            } else if (menuScreen.isButtonPressed(pauseButtons.restartButtonRectangle)) {
                gameView.setGameState(GameState.WELCOME);

            }
        }
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchCancelled(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        if (gameView.getGameState() == GameState.WELCOME) {
            if (menuScreen.isMouseOverButton(menuScreen.playButtonRectangle)) {
                menuScreen.setButtonState(menuScreen.playButtonRectangle, true);
                menuScreen.setButtonState(menuScreen.exitButtonRectangle, false);
                menuScreen.setButtonState(menuScreen.controlButtonRectangle, false);
            } else if (menuScreen.isMouseOverButton(menuScreen.exitButtonRectangle)) {
                menuScreen.setButtonState(menuScreen.exitButtonRectangle, true);
                menuScreen.setButtonState(menuScreen.playButtonRectangle, false);
                menuScreen.setButtonState(menuScreen.controlButtonRectangle, false);
            } else if (menuScreen.isMouseOverButton(menuScreen.controlButtonRectangle)) {
                menuScreen.setButtonState(menuScreen.controlButtonRectangle, true);
                menuScreen.setButtonState(menuScreen.playButtonRectangle, false);
                menuScreen.setButtonState(menuScreen.exitButtonRectangle, false);
            } else {
                menuScreen.setButtonState(menuScreen.playButtonRectangle, false);
                menuScreen.setButtonState(menuScreen.exitButtonRectangle, false);
                menuScreen.setButtonState(menuScreen.controlButtonRectangle, false);
            }
        }
        if (gameView.getGameState() == GameState.PLAYING) {
            if (menuScreen.isMouseOverButton(pauseButtons.pauseButtonRectangle)) {
                pauseButtons.pauseButtonState(pauseButtons.pauseButtonRectangle, true);
            } else {
                pauseButtons.pauseButtonState(pauseButtons.pauseButtonRectangle, false);
            }
        }
        if (gameView.getGameState() == GameState.PAUSED) {
            if (menuScreen.isMouseOverButton(pauseButtons.continueButtonRectangle)) {
                pauseButtons.pauseButtonState(pauseButtons.continueButtonRectangle, true);
                pauseButtons.pauseButtonState(pauseButtons.exitButtonRectangle, false);
                pauseButtons.pauseButtonState(pauseButtons.restartButtonRectangle, false);
            } else if (menuScreen.isMouseOverButton(pauseButtons.exitButtonRectangle)) {
                pauseButtons.pauseButtonState(pauseButtons.exitButtonRectangle, true);
                pauseButtons.pauseButtonState(pauseButtons.continueButtonRectangle, false);
                pauseButtons.pauseButtonState(pauseButtons.restartButtonRectangle, false);
            } else if (menuScreen.isMouseOverButton(pauseButtons.restartButtonRectangle)) {
                pauseButtons.pauseButtonState(pauseButtons.restartButtonRectangle, true);
                pauseButtons.pauseButtonState(pauseButtons.continueButtonRectangle, false);
                pauseButtons.pauseButtonState(pauseButtons.exitButtonRectangle, false);
            } else {
                pauseButtons.pauseButtonState(pauseButtons.continueButtonRectangle, false);
                pauseButtons.pauseButtonState(pauseButtons.exitButtonRectangle, false);
                pauseButtons.pauseButtonState(pauseButtons.restartButtonRectangle, false);
            }
        }
        return false;
    }

    @Override
    public boolean scrolled(float amountX, float amountY) {
        return false;
    }

    public void fromAirToStandingStill() {
        if ((body.getLinearVelocity().x == 0 && body.getLinearVelocity().y == 0) &&
                (player.getPlayerState() != PlayerState.SHOOTING_RIGHT) &&
                (player.getPlayerState() != PlayerState.SHOOTING_LEFT)) {
            if (player.getPlayerState() == PlayerState.JUMPING_RIGHT ||
                    player.getPlayerState() == PlayerState.RUNNING_RIGHT ||
                    player.getPlayerState() == PlayerState.STANDING_RIGHT ||
                    player.getPlayerState() == PlayerState.SHOOTING_RIGHT ||
                    player.getPlayerState() == PlayerState.SHOOTING_MID_AIR_RIGHT) {
                player.setPlayerState(PlayerState.STANDING_RIGHT);
            } else {
                player.setPlayerState(PlayerState.STANDING_LEFT);
            }
        }
    }

    public void moveRight(boolean movingRight) {
        if (movingRight) {
            body.applyLinearImpulse(new Vector2(body.getMass() * 100, 0), body.getWorldCenter(), true);
        } else {
            body.setLinearVelocity(0, 0);
        }
    }

    private void moveLeft(boolean movingLeft) {
        if (movingLeft) {
            body.applyLinearImpulse(new Vector2(body.getMass() * -100, 0),
                    body.getWorldCenter(), true);
        } else {
            body.setLinearVelocity(0f, 0);
        }
    }

    public Fireball getFireball() {
        return fireball;
    }

    @Override
    public void beginContact(Contact contact) {
        Body bodyA = contact.getFixtureA().getBody();
        Body bodyB = contact.getFixtureB().getBody();
        Object fixtureA = bodyA.getUserData();
        Object fixtureB = bodyB.getUserData();
        if (fixtureA.equals("player") && fixtureB.equals("ground")) {
            groundContacts++;
            ableToJump = true;
            if (bodyA.getLinearVelocity().x >= 0) {
                player.setPlayerState(PlayerState.RUNNING_RIGHT);
            } else if (bodyA.getLinearVelocity().x <= 0) {
                player.setPlayerState(PlayerState.RUNNING_LEFT);
            }
        }
        if (fixtureA.equals("player") && fixtureB.equals("enemies") && groundContacts > 0) {
            gameView.setGameState(GameState.GAME_OVER);
        } else if (fixtureA.equals("player") && fixtureB.equals("enemies") && groundContacts == 0) {
            bodyA.applyLinearImpulse(new Vector2(0, body.getMass() * 120), body.getWorldCenter(), true);
        }
        if (fixtureA.equals("player") && fixtureB.equals("obstacle")) {
            gameView.setGameState(GameState.GAME_OVER);
        }
        if (fixtureA.equals("enemies") && fixtureB.equals("fireball")) {
            enemy.setElapsedTimeEnemy(1 / 8f * 10);
            enemy.removeEnemy(bodyA, world);
            world.step(0, 0, 0);
            world.destroyBody(bodyA);
            world.step(Gdx.graphics.getDeltaTime(), 6, 2);
        }
        if (fixtureA.equals("ground") && fixtureB.equals("fireball")) {
            fireball.removeFireball(bodyB);
        }
    }

    @Override
    public void endContact(Contact contact) {
        Body bodyA = contact.getFixtureA().getBody();
        Body bodyB = contact.getFixtureB().getBody();
        Object fixtureA = bodyA.getUserData();
        Object fixtureB = bodyB.getUserData();
        if (fixtureA.equals("player") && fixtureB.equals("ground")) {
            groundContacts--;
            if (groundContacts <= 0) {
                ableToJump = false;
            }
        }
    }

    @Override
    public void preSolve(Contact contact, Manifold oldManifold) {
    }

    @Override
    public void postSolve(Contact contact, ContactImpulse impulse) {
    }
}
