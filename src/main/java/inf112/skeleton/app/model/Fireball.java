package inf112.skeleton.app.model;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;

public class Fireball {
    private Body body;
    public static Array<Body> fireBallArray = new Array<>();
    public static Array<Body> fireBallToBeRemoved = new Array<>();

    public Fireball(World world, Body Box2DPlayer, Player player) {
        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyType.DynamicBody;
        if (player.getPlayerState() == PlayerState.RUNNING_RIGHT ||
                player.getPlayerState() == PlayerState.STANDING_RIGHT ||
                player.getPlayerState() == PlayerState.SHOOTING_RIGHT ||
                player.getPlayerState() == PlayerState.SHOOTING_MID_AIR_RIGHT ||
                player.getPlayerState() == PlayerState.JUMPING_RIGHT) {
            bodyDef.position.set(Box2DPlayer.getPosition().x + 12, Box2DPlayer.getPosition().y);
        } else {
            bodyDef.position.set(Box2DPlayer.getPosition().x - 12, Box2DPlayer.getPosition().y);
        }

        bodyDef.fixedRotation = true;

        PolygonShape shape = new PolygonShape();
        shape.setAsBox(2, 2);

        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = shape;
        fixtureDef.density = 0f;
        fixtureDef.friction = 0f;
        fixtureDef.restitution = 0f;

        body = world.createBody(bodyDef);
        body.setUserData("fireball");
        body.createFixture(fixtureDef);

        shape.dispose();
    }

    public void renderFireball(Fireball fireball, SpriteBatch batch) {
        Texture fireballTexture = new Texture(Gdx.files.internal("fireball.png"));
        for (Body fireBallBody : fireBallArray) {
            batch.draw(fireballTexture, fireBallBody.getPosition().x - 1, fireBallBody.getPosition().y - 1);
        }
    }

    public Body getFireBall() {
        return body;
    }

    public void addFireBall(Body fireBallBody) {
        fireBallArray.add(fireBallBody);
    }

    public void removeFireball(Body fireBallBody) {
        for (int i = 0; i < fireBallArray.size; i++) {
            if (fireBallArray.get(i) == fireBallBody) {
                fireBallToBeRemoved.add(fireBallArray.get(i));
                fireBallArray.removeIndex(i);
            }
        }
    }

    public void moveFireball(Box2DPlayer box2dPlayer) {
        Body fireBallBody = getFireBall();
        if (box2dPlayer.getBody().getPosition().x < fireBallBody.getPosition().x) {
            fireBallBody.setLinearVelocity(100, 0);
        } else {
            fireBallBody.setLinearVelocity(-100, 0);
        }
    }
}
