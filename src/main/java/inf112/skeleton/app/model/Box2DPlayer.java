package inf112.skeleton.app.model;

import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;

import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;

/**
 * Represents the player in the game, handling player-specific properties such
 * as texture and animation,
 * along with the physics body for collision and movement within the Box2D
 * world.
 */
public class Box2DPlayer {
    private Body body;

    /**
     * Constructs a player and initializes its physical body within the specified
     * Box2D world.
     * 
     * @param world The Box2D world where the player's body is created.
     */
    public Box2DPlayer(World world) {
        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyType.DynamicBody;
        bodyDef.position.set(32, 55);
        bodyDef.fixedRotation = true;

        PolygonShape shape = new PolygonShape();
        shape.setAsBox(8, 16);

        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = shape;
        fixtureDef.density = 2.5f;
        fixtureDef.restitution = 0;
        fixtureDef.friction = 0f;

        body = world.createBody(bodyDef);
        body.setUserData("player");
        body.createFixture(fixtureDef);

        shape.dispose();
    }

    /**
     * Resets the position of a body to start position of the map.
     */
    public void setPosition() {
        body.setTransform(32, 55, 0);
    }

    /**
     * Retrieves the Box2D body associated with the player.
     * 
     * @return The Box2D {@link Body} representing the player.
     */
    public Body getBody() {
        return body;
    }

    /**
     * Checks if the player is within the bounds of the game world to prevent game
     * over.
     * 
     * @param world The Box2D world to compare the player's position against.
     * @return {@code true} if the player is within the world bounds; {@code false}
     *         otherwise.
     */
    public boolean insideWorld(World world, TiledMap map) {
        int mapWidth = map.getProperties().get("width", Integer.class);
        float playerPosX = body.getPosition().x / 32;
        float playerPosY = body.getPosition().y / 32;
        if (playerPosX < -1 || playerPosX > mapWidth || playerPosY < 0) {
            return false;
        }
        return true;
    }
}