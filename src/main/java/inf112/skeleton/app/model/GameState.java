package inf112.skeleton.app.model;

public enum GameState {
    WELCOME,
    PLAYING,
    GAME_OVER,
    WINNER,
    CONTROLS,
    EXIT,
    PAUSED
}
