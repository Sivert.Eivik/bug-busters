package inf112.skeleton.app.model;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.MapObjects;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.physics.box2d.World;
import inf112.skeleton.app.view.GameView;

/**
 * Represents an enemy in the game, handling enemy-specific behaviors such as
 * spawning and automatic movement.
 * This class delegates to a {@link MarioModel} instance for physical body
 * creation and movement logic.
 */
public class Enemy {
    private MarioModel model;
    private GameView gameView;
    private Rectangle spriteRect = new Rectangle(0, 0, 32, 32);
    private TextureAtlas textureAtlas = new TextureAtlas(Gdx.files.internal("death_and_walk_animation.atlas"));;
    private Animation<TextureRegion> animation = new Animation<>(1 / 8f, textureAtlas.getRegions());;
    private float deathElapsedTime = 1 / 8f * 10;
    private float elapsedTime = 0;
    private static final float ENEMYMOVESPEED = -15;
    private Array<Body> enemies;
    public static Array<Body> deadEnemies = new Array<>();

    /**
     * Constructs an enemy instance for physics and movement logic.
     * 
     * @param model The instance used for enemy physics body creation and movement.
     */
    public Enemy(MarioModel model, GameView gameView) {
        this.model = model;
        this.gameView = gameView;
    }

    public Array<Body> enemyArray(TiledMap map, World world, String name, BodyType bodyType, String userData) {
        enemies = new Array<Body>();
        MapObjects enemyObjects = map.getLayers().get(name).getObjects();
        for (int i = 0; i < enemyObjects.getCount(); i++) {
            float x = enemyObjects.get(i).getProperties().get("x", float.class);
            float y = enemyObjects.get(i).getProperties().get("y", float.class);
            Body enemyBody = model.bodyCreation(map, world, name, bodyType, userData);
            enemyBody.setTransform(x, y + 16, 0);
            enemies.add(enemyBody);
        }
        return enemies;
    }

     /**
     * Returns the current list of enemies.
     * This method can be useful for testing or when external classes need to read but not modify the enemies.
     * @return A read-only view of the enemies array.
     */
    public Array<Body> getEnemies() {
        return enemies;
    }

    public void render(Array<Body> enemies, SpriteBatch batch) {
        elapsedTime += Gdx.graphics.getDeltaTime();
        for (Body enemy : enemies) {
            float x = enemy.getPosition().x - spriteRect.getWidth() / 2;
            float y = enemy.getPosition().y - spriteRect.getHeight() / 2;
            if (elapsedTime > 1 / 8f * 10) {
                elapsedTime = 0;
            }
            batch.draw(animation.getKeyFrame(elapsedTime, true), x, y, spriteRect.getHeight(), spriteRect.getWidth());
        }
    }

    public void renderDeathAnimation(SpriteBatch batch) {
        deathElapsedTime += Gdx.graphics.getDeltaTime();
        for (Body enemyBody : deadEnemies) {
            float x = enemyBody.getPosition().x - spriteRect.getWidth() / 2;
            float y = enemyBody.getPosition().y - spriteRect.getHeight() / 2;
            if (deathElapsedTime < 1 / 8f * 16) {
                batch.draw(animation.getKeyFrame(deathElapsedTime, true), x, y, spriteRect.getHeight(),
                        spriteRect.getWidth());
            }
            if (deathElapsedTime >= 1 / 8f * 16) {
                batch.draw(animation.getKeyFrame(1 / 8f * 16, false), x, y, spriteRect.getHeight(),
                        spriteRect.getWidth());
                deadEnemies.removeIndex(0);
            }
        }
    }

    public void setElapsedTimeEnemy(float elapsedTimeEnemy) {
        deathElapsedTime = elapsedTimeEnemy;
    }

    public void updateEnemyArray(Array<Body> enemies) {
        for (int i = 0; i < enemies.size; i++) {
            Body enemy = enemies.get(i);
            if (enemy.getPosition().x < 0 || enemy.getPosition().y < 0) {
                enemies.removeIndex(i);
            }
        }
    }

    public void removeEnemy(Body enemy, World world) {
        for (int i = 0; i < enemies.size; i++) {
            if (enemies.get(i) == enemy) {
                deadEnemies.add(enemy);
                enemies.removeIndex(i);
            }
        }
    }

    public void enemyMovement() {
        for (Body body : enemies) {
            if (gameView.getGameState() == GameState.PLAYING) {
                body.setLinearVelocity(ENEMYMOVESPEED, -body.getWorldCenter().y);
            } else {
                body.setLinearVelocity(0, 0);
            }
        }
    }
}
