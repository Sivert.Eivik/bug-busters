package inf112.skeleton.app.model;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import inf112.skeleton.app.view.GameView;

public class GameSounds {
    private GameView gameView;
    private boolean gameOver = false;
    private boolean victory = false;
    private static final Music backgroundMusic = Gdx.audio.newMusic(Gdx.files.internal("background.mp3"));
    private static final Music gameOverMusic = Gdx.audio.newMusic(Gdx.files.internal("gameover.mp3"));
    private static final Music victoryMusic = Gdx.audio.newMusic(Gdx.files.internal("victory.mp3"));

    public GameSounds(GameView gameView) {
        this.gameView = gameView;
    }

    public void playMusic() {
        if (gameView.getGameState() == GameState.PLAYING) {
            backgroundMusic.play();
        } else if (gameView.getGameState() == GameState.GAME_OVER && !gameOver) {
            backgroundMusic.stop();
            gameOverMusic.play();
            gameOver = true;
        } else if (gameView.getGameState() == GameState.WINNER && !victory) {
            backgroundMusic.stop();
            victoryMusic.play();
            victory = true;
        } else if (backgroundMusic.isPlaying() && gameView.getGameState() != GameState.PLAYING) {
            backgroundMusic.stop();
        }
    }
}
