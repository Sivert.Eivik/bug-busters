package inf112.skeleton.app.model;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Scaling;
import com.badlogic.gdx.utils.viewport.ScalingViewport;
import inf112.skeleton.app.levels.LevelHandler;

public class MenuScreen {
    private Stage stage;
    private Texture controlScreen;
    private Texture background;

    public boolean playButtonDown = false;
    public boolean controlButtonDown = false;
    public boolean exitButtonDown = false;

    private float screenWidth = Gdx.graphics.getWidth();
    private float screenHeight = Gdx.graphics.getHeight();

    private static final int pngbutton_width = 757; // Bredde på hver knapp i png filen
    private static final int pngbutton_height = 345; // Høyden på hver knapp i png filen
    private TextureRegion playButtonTexture;
    private TextureRegion controlButtonTexture;
    private TextureRegion exitButtonTexture;

    public Rectangle playButtonRectangle;
    public Rectangle controlButtonRectangle;
    public Rectangle exitButtonRectangle;

    private TextureRegion[][] buttonTextures;

    public MenuScreen(SpriteBatch batch) {
        background = new Texture(Gdx.files.internal("bakgrunn.png"));
        controlScreen = new Texture(Gdx.files.internal("controlScreen.png"));

        Texture buttonsTexture = new Texture(Gdx.files.internal("buttons.png"));
        buttonTextures = TextureRegion.split(buttonsTexture, pngbutton_width, pngbutton_height);

        playButtonRectangle = new Rectangle((screenWidth / 2) - 100, (screenHeight / 2) + 50, 200, 100);
        controlButtonRectangle = new Rectangle((screenWidth / 2) - 100,
                playButtonRectangle.y - playButtonRectangle.height, 200, 100);
        exitButtonRectangle = new Rectangle((screenWidth / 2) - 100,
                controlButtonRectangle.y - controlButtonRectangle.height, 200, 100);
        loadTextures();
        stage = new Stage(new ScalingViewport(Scaling.stretch, Gdx.graphics.getWidth(), Gdx.graphics.getHeight(),
                new OrthographicCamera()), batch);
    }

    private void loadTextures() {
        playButtonTexture = playButtonDown ? buttonTextures[0][1] : buttonTextures[0][0];
        controlButtonTexture = controlButtonDown ? buttonTextures[1][1] : buttonTextures[1][0];
        exitButtonTexture = exitButtonDown ? buttonTextures[2][1] : buttonTextures[2][0];
    }

    /**
     * Draws buttons on the screen using the provided SpriteBatch.
     *
     * @param batch The SpriteBatch used for rendering.
     */
    private void drawButtons(SpriteBatch batch, LevelHandler levelHandler) {
        int buttonWidth = 200;
        int buttonHeight = 100;
        Vector3 cameraPosition = levelHandler.currentCameraPosition();
        float playButtonX = cameraPosition.x - playButtonRectangle.getWidth() / 2;
        float playButtonY = cameraPosition.y + playButtonRectangle.getHeight() / 2;
        batch.draw(playButtonTexture, playButtonX, playButtonY, buttonWidth, buttonHeight);
        float controlButtonX = cameraPosition.x - controlButtonRectangle.getWidth() / 2;
        float controlButtonY = cameraPosition.y - controlButtonRectangle.getHeight() / 2;
        batch.draw(controlButtonTexture, controlButtonX, controlButtonY, buttonWidth, buttonHeight);
        float exitButtonX = cameraPosition.x - exitButtonRectangle.getWidth() / 2;
        float exitButtonY = cameraPosition.y - exitButtonRectangle.getHeight() * 1.5f;
        batch.draw(exitButtonTexture, exitButtonX, exitButtonY, buttonWidth, buttonHeight);
    }

    /**
     * Sets the state of the specified button to either hovered (down) or not
     * hovered (up).
     *
     * @param button       The Rectangle representing the button.
     * @param isButtonDown The state to set the button to (true for hovered, false
     *                     for not hovered).
     */
    public void setButtonState(Rectangle button, boolean isButtonDown) {
        if (button == playButtonRectangle) {
            playButtonDown = isButtonDown;
        } else if (button == exitButtonRectangle) {
            exitButtonDown = isButtonDown;
        } else if (button == controlButtonRectangle) {
            controlButtonDown = isButtonDown;
        }
        loadTextures();
    }

    /**
     * Checks if the specified button is pressed (touched) on the screen.
     *
     * @param button The Rectangle representing the button to check.
     * @return true if the button is pressed, false otherwise.
     */
    public boolean isButtonPressed(Rectangle button) {
        if (Gdx.input.isTouched()) {
            float screenX = Gdx.input.getX();
            float screenY = Gdx.input.getY();
            Vector3 touchPos = new Vector3(screenX, screenY, 0);
            stage.getCamera().unproject(touchPos);
            return button.contains(touchPos.x, touchPos.y);
        }
        return false;
    }

    /**
     * Checks if the mouse pointer is over the specified button.
     *
     * @param button The Rectangle representing the button to check.
     * @return true if the mouse pointer is over the button, false otherwise.
     */
    public boolean isMouseOverButton(Rectangle button) {
        float screenX = Gdx.input.getX();
        float screenY = Gdx.input.getY();

        Vector3 touchPos = new Vector3(screenX, screenY, 0);
        stage.getCamera().unproject(touchPos);

        return button.contains(touchPos.x, touchPos.y);
    }

    /**
     * Draws the welcome screen elements on the specified SpriteBatch.
     *
     * @param batch The SpriteBatch used for drawing.
     */
    public void drawWelcomeScreen(SpriteBatch batch, LevelHandler levelHandler) {
        float x = levelHandler.currentCameraPosition().x - background.getWidth() / 2;
        batch.draw(background, x, 0, screenWidth, screenHeight);
        drawButtons(batch, levelHandler);
    }

    /**
     * Draws the controls screen on the specified SpriteBatch.
     *
     * @param batch The SpriteBatch used for drawing.
     */
    public void drawControlsScreen(SpriteBatch batch, LevelHandler levelHandler) {
        float x = levelHandler.currentCameraPosition().x - controlScreen.getWidth() / 5.0625f;
        batch.draw(controlScreen, x, 0, screenWidth, screenHeight);
    }

    /**
     * Updates the stage by the specified time delta.
     *
     * @param delta The time delta for the update, in seconds.
     */
    public void update(float delta) {
        stage.act(delta);
    }

}