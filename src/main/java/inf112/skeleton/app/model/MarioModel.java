package inf112.skeleton.app.model;

import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;

public class MarioModel implements IModel {
    private Body body;

    @Override
    public Body bodyCreation(TiledMap map, World world, String name, BodyType bodyType, String userData) {
        BodyDef bdef = new BodyDef();
        PolygonShape polygonShape = new PolygonShape();
        FixtureDef fixtureDef = new FixtureDef();
        for (MapObject mapObject : map.getLayers().get(name).getObjects()) {
            RectangleMapObject rectangleObject = (RectangleMapObject) mapObject;
            Rectangle rectangle = rectangleObject.getRectangle();

            float x = (rectangle.getX() + rectangle.getWidth() / 2);
            float y = (rectangle.getY() + rectangle.getHeight() / 2);

            bdef.type = bodyType;
            if (name != "enemies") {
                bdef.position.set(x, y);
            }
            body = world.createBody(bdef);
            body.setUserData(userData);
            polygonShape.setAsBox(rectangle.getWidth() / 2, rectangle.getHeight() / 2);
            fixtureDef.shape = polygonShape;
            fixtureDef.density = 2.5f;
            fixtureDef.friction = 0f;
            fixtureDef.restitution = 0f;
            body.createFixture(fixtureDef);
        }
        return body;
    }
}
