package inf112.skeleton.app.model;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;

public class PauseButtons {
    public boolean pauseButtonDown = false;
    public boolean continueDown = false;
    public boolean exitDown = false;
    public boolean restartDown = false;
    private float screenWidth = Gdx.graphics.getWidth();
    private float screenHeight = Gdx.graphics.getHeight();

    private TextureRegion pauseButtonTexture;
    private Texture pauseMenuTexture;
    private TextureRegion continueButtonTexture;
    private TextureRegion exitButtonTexture;
    private TextureRegion restartButtonTexture;

    private static final int png_width = 1788 / 2; // Bredde på hver knapp i png filen
    private static final int png_height = 2076 / 4; // Høyden på hver knapp i png filen

    public Rectangle pauseMenuRectangle;
    public Rectangle pauseButtonRectangle;
    public Rectangle continueButtonRectangle;
    public Rectangle exitButtonRectangle;
    public Rectangle restartButtonRectangle;

    private TextureRegion[][] pauseMenuButtons;

    MenuScreen menuScreen;

    public PauseButtons(MenuScreen menuScreen) {
        this.menuScreen = menuScreen;
        pauseMenuTexture = new Texture(Gdx.files.internal("pauseBoard.png"));
        Texture pauseButtonsTexture = new Texture(Gdx.files.internal("pauseKnapper.png"));

        pauseMenuButtons = TextureRegion.split(pauseButtonsTexture, png_width, png_height);

        pauseMenuRectangle = new Rectangle((screenWidth / 2) - 200, (screenHeight / 2) - 200, 400, 400);

        restartButtonRectangle = new Rectangle((screenWidth / 2) - 70, (screenHeight / 2) - 55, 140, 100);

        continueButtonRectangle = new Rectangle(restartButtonRectangle.x - 100, restartButtonRectangle.y, 140, 100);

        exitButtonRectangle = new Rectangle(restartButtonRectangle.x + 100, restartButtonRectangle.y, 140, 100);

        pauseButtonRectangle = new Rectangle(0, screenHeight - 55, 90, 60);

        getPauseMenuTextures();

    }

    private void getPauseMenuTextures() {
        pauseButtonTexture = pauseButtonDown ? pauseMenuButtons[3][1] : pauseMenuButtons[3][0];
        continueButtonTexture = continueDown ? pauseMenuButtons[0][1] : pauseMenuButtons[0][0];
        restartButtonTexture = restartDown ? pauseMenuButtons[2][1] : pauseMenuButtons[2][0];
        exitButtonTexture = exitDown ? pauseMenuButtons[1][1] : pauseMenuButtons[1][0];
    }

    /**
     * Draws the pause menu with its buttons using the specified SpriteBatch.
     *
     * @param batch The SpriteBatch used for drawing.
     */
    public void drawPauseMenu(SpriteBatch batch, Vector3 cameraPosition) {
        float pauseMenuX = cameraPosition.x - pauseMenuRectangle.getWidth()/2;
        float pauseMenuY = cameraPosition.y - pauseMenuRectangle.getHeight()/2;
        batch.draw(pauseMenuTexture, pauseMenuX, pauseMenuY, pauseMenuRectangle.width,
                pauseMenuRectangle.height);
        float continueButtonX = cameraPosition.x - continueButtonRectangle.getWidth()*1.25f;
        batch.draw(continueButtonTexture, continueButtonX, continueButtonRectangle.y,
                continueButtonRectangle.width,
                continueButtonRectangle.height);
        float restartButtonX = cameraPosition.x - restartButtonRectangle.getWidth()/2;
        batch.draw(restartButtonTexture, restartButtonX, restartButtonRectangle.y,
                restartButtonRectangle.width, restartButtonRectangle.height);
        float exitButtonX = cameraPosition.x + exitButtonRectangle.getWidth()/4;
        batch.draw(exitButtonTexture, exitButtonX, exitButtonRectangle.y, exitButtonRectangle.width,
                exitButtonRectangle.height);
    }

    /**
     * Draws the pause button using the specified SpriteBatch.
     *
     * @param batch The SpriteBatch used for drawing.
     */
    public void drawPauseButton(SpriteBatch batch, OrthographicCamera camera) {
        float x = camera.position.x - camera.viewportWidth / 2;
        float y = pauseButtonRectangle.y;
        batch.draw(pauseButtonTexture, x, y, pauseButtonRectangle.width,
                pauseButtonRectangle.height);
    }

    /**
     * Sets the state of the specified button to either pressed or not pressed.
     *
     * @param button       The Rectangle representing the button.
     * @param isButtonDown True if the button is pressed, false otherwise.
     */
    public void pauseButtonState(Rectangle button, boolean isButtonDown) {
        if (button == pauseButtonRectangle) {
            pauseButtonDown = isButtonDown;
        } else if (button == exitButtonRectangle) {
            exitDown = isButtonDown;
        } else if (button == continueButtonRectangle) {
            continueDown = isButtonDown;
        } else if (button == restartButtonRectangle) {
            restartDown = isButtonDown;
        }
        getPauseMenuTextures();
    }
}
