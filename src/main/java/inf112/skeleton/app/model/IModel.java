package inf112.skeleton.app.model;

import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.World;

public interface IModel {
    /**
     * 
     * Initializes and adds physical bodies to the world from a specified map layer.
     * 
     * @param map      The tiled map containing layers of objects.
     * @param world    The physics world where bodies are created.
     * @param name     The layer name in the map to look for objects.
     * @param bodyType The type of physics bodies to create (dynamic, static)
     */
    Body bodyCreation(TiledMap map, World world, String name, BodyType bodyType, String userData);
}
