package inf112.skeleton.app.model;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.physics.box2d.Body;

public class Player {

    private Rectangle spriteRect;
    private TextureAtlas textureAtlas;
    private Animation<TextureRegion> animation;

    private float elapsedTime = 0f;
    private float framesPerSecond = 1f / 8f;

    private final float STANDING_RIGHT_ANIMATION = 0;
    private final float STANDING_LEFT_ANIMATION = framesPerSecond * 18;
    private final float STARTFRAME_RUNNING_RIGHT = framesPerSecond * 1;
    private final float STARTFRAME_RUNNING_LEFT = framesPerSecond * 19;
    private final float STARTFRAME_JUMPING_RIGHT = framesPerSecond * 7;
    private final float STARTFRAME_JUMPING_LEFT = framesPerSecond * 25;
    private final float STARTFRAME_SHOOTING_RIGHT = framesPerSecond * 12;
    private final float STARTFRAME_SHOOTING_LEFT = framesPerSecond * 30;
    private final float STARTFRAME_SHOOTING_MID_AIR_RIGHT = framesPerSecond * 15;
    private final float STARTFRAME_SHOOTING_MID_AIR_LEFT = framesPerSecond * 33;

    private PlayerState playerState;

    public Player(PlayerState playerState) {
        spriteRect = new Rectangle(0, 0, 32, 32);
        textureAtlas = new TextureAtlas(Gdx.files.internal("animationAtlas.atlas"));
        animation = new Animation<>(framesPerSecond, textureAtlas.getRegions());
        this.playerState = playerState;
    }

    public void renderPlayerAnimations(SpriteBatch batch, Body box2DPlayer) {
        if (playerState == PlayerState.JUMPING_RIGHT) {
            getAnimationFrames(7, 10);
        }

        else if (playerState == PlayerState.JUMPING_LEFT) {
            getAnimationFrames(25, 28);
        }

        else if (playerState == PlayerState.STANDING_LEFT) {
            elapsedTime = STANDING_LEFT_ANIMATION;
        }

        else if (playerState == PlayerState.RUNNING_RIGHT) {
            getAnimationFrames(1, 6);
        }

        else if (playerState == PlayerState.RUNNING_LEFT) {
            getAnimationFrames(19, 24);
        }

        else if (playerState == PlayerState.STANDING_RIGHT) {
            elapsedTime = STANDING_RIGHT_ANIMATION;
        }

        else if (playerState == PlayerState.SHOOTING_RIGHT) {
            getShootingFrames(14, PlayerState.RUNNING_RIGHT);
        }

        else if (playerState == PlayerState.SHOOTING_LEFT) {
            getShootingFrames(32, PlayerState.RUNNING_LEFT);
        }

        else if (playerState == PlayerState.SHOOTING_MID_AIR_RIGHT) {
            getShootingFrames(17, PlayerState.JUMPING_RIGHT);
        }

        else if (playerState == PlayerState.SHOOTING_MID_AIR_LEFT) {
            getShootingFrames(35, PlayerState.JUMPING_LEFT);
        }

        batch.draw(animation.getKeyFrame(elapsedTime, true), box2DPlayer.getPosition().x - (spriteRect.getWidth() / 2),
                box2DPlayer.getPosition().y - (spriteRect.getHeight() / 2));
    }

    /**
     * 
     * @param startFrame startFrame from animationAtlas.png
     * @param EndFrame   endFrame from animationAtlas.png
     *                   Viser hvert bilde mellom startFrame og endFrame i en loop.
     */
    private void getAnimationFrames(float startFrame, float EndFrame) {
        float num = 0.1f;

        elapsedTime += Gdx.graphics.getDeltaTime();
        if (elapsedTime > framesPerSecond * EndFrame + num) {
            elapsedTime = framesPerSecond * startFrame;
        }

    }

    /**
     * @param endFrame    last frame in the animation
     * @param playerState after the animation, change to the given playerstate
     * 
     *                    different from getAnimationFrames because the shooting
     *                    animation should not be shown
     *                    more than one time. After that, change back to the
     *                    previous playerstate.
     */
    private void getShootingFrames(float endFrame, PlayerState playerState) {
        float num = 0.1f;

        elapsedTime += Gdx.graphics.getDeltaTime();
        if (elapsedTime > framesPerSecond * endFrame + num) {
            setPlayerState(playerState);
        }
    }

    public void setPlayerState(PlayerState playerState) {
        this.playerState = playerState;
        setStartFrame(playerState);
    }

    public PlayerState getPlayerState() {
        return this.playerState;
    }

    private float setStartFrame(PlayerState playerState) {
        if (playerState == PlayerState.JUMPING_RIGHT) {
            elapsedTime = STARTFRAME_JUMPING_RIGHT;
        } else if (playerState == PlayerState.JUMPING_LEFT) {
            elapsedTime = STARTFRAME_JUMPING_LEFT;
        } else if (playerState == PlayerState.RUNNING_RIGHT) {
            elapsedTime = STARTFRAME_RUNNING_RIGHT;
        } else if (playerState == PlayerState.RUNNING_LEFT) {
            elapsedTime = STARTFRAME_RUNNING_LEFT;
        } else if (playerState == PlayerState.SHOOTING_MID_AIR_RIGHT) {
            elapsedTime = STARTFRAME_SHOOTING_MID_AIR_RIGHT;
        } else if (playerState == PlayerState.SHOOTING_MID_AIR_LEFT) {
            elapsedTime = STARTFRAME_SHOOTING_MID_AIR_LEFT;
        } else if (playerState == PlayerState.SHOOTING_RIGHT) {
            elapsedTime = STARTFRAME_SHOOTING_RIGHT;
        } else if (playerState == PlayerState.SHOOTING_LEFT) {
            elapsedTime = STARTFRAME_SHOOTING_LEFT;
        }
        return elapsedTime;
    }

}
