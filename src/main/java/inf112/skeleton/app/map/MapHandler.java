package inf112.skeleton.app.map;

import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;

public class MapHandler {
    private TmxMapLoader mapLoader;
    private TiledMap tiledMap;

    public TiledMap loadMap(String stringPath){
        mapLoader = new TmxMapLoader();
        tiledMap = mapLoader.load(stringPath);
        return tiledMap;
    }
}
