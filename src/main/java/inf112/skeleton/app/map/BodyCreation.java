package inf112.skeleton.app.map;

import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.World;
import inf112.skeleton.app.model.MarioModel;

public class BodyCreation{
    /**
     * Creates a body for objects in the map.
     * @param map - current map
     * @param world - current world
     * @param name - name of object
     * @param bodyType - bodyType for object
     * @param userData - userdata of object
     * @return - body created
     */
    public Body createBody(TiledMap map, World world, String name, BodyDef.BodyType bodyType, String userData) {
        MarioModel model = new MarioModel();
        return(model.bodyCreation(map, world, name, bodyType, userData));
    }
}
