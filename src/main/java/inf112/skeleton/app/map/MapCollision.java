package inf112.skeleton.app.map;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import inf112.skeleton.app.model.Box2DPlayer;

public class MapCollision {
    private static final int TILESIZE = 32;
    private static final float FINISHTILE = 278;
    private List<Float> finishPosition;

    /**
     * Checks if player has reached finish tile or not
     * 
     * @param player - main character
     * @param map    - current Tiled map
     * @return - true if player has reached finished tile, else false
     */
    public boolean finishCollision(Box2DPlayer player, TiledMap map) {
        float finish_X = finishPosition.get(0);
        float finish_Y = finishPosition.get(1);
        int playerPositionX = (int) player.getBody().getPosition().x / TILESIZE;
        int playerPositionY = (int) player.getBody().getPosition().y / TILESIZE;
        if (playerPositionX == finish_X && playerPositionY == finish_Y) {
            return true;
        }
        return false;
    }

    /**
     * Updates the finish tile for when a new map is rendered
     * 
     * @param map - current map
     */
    public List<Float> updateFinishCollision(TiledMap map) {
        finishPosition = new ArrayList<>();
        TiledMapTileLayer finishLayer = (TiledMapTileLayer) map.getLayers().get("finish_line");
        for (int y = 0; y < finishLayer.getHeight(); y++) {
            for (int x = 0; x < finishLayer.getWidth(); x++) {
                TiledMapTileLayer.Cell cell = finishLayer.getCell(x, y);
                if (cell != null && cell.getTile().getId() == FINISHTILE) {
                    finishPosition.add((float) x);
                    finishPosition.add((float) y);
                    break;
                }
            }
        }
        return finishPosition;
    }
}
