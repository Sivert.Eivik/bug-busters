package inf112.skeleton.app;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyFloat;
import static org.mockito.Mockito.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.headless.HeadlessApplication;
import com.badlogic.gdx.backends.headless.HeadlessApplicationConfiguration;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.WorldDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.utils.Array;

import inf112.skeleton.app.levels.Level1;
import inf112.skeleton.app.model.Enemy;
import inf112.skeleton.app.model.GameState;
import inf112.skeleton.app.model.MarioModel;
import inf112.skeleton.app.view.GameView;

public class EnemyTest {
    private Enemy enemy;
    private TiledMap map;
    private World world;
    private MarioModel model;
    private GameView gameView;
    private Array<Body> enemyArray;
    private SpriteBatch mockBatch;

    @BeforeEach
    void setUp() {
        HeadlessApplicationConfiguration cfg = new HeadlessApplicationConfiguration();
        ApplicationListener applicationListener = new ApplicationAdapter() {
        };
        Gdx.gl20 = mock(GL20.class);
        Gdx.gl = mock(GL20.class);
        new HeadlessApplication(applicationListener, cfg);
        world = new World(new WorldDef(new Vector2(0, -9.81f), true));
        model = new MarioModel();
        gameView = mock(GameView.class);
        enemy = new Enemy(model, gameView);
        Level1 level1 = new Level1();
        map = level1.map();
        enemyArray = enemy.enemyArray(map, world, "enemies", BodyType.DynamicBody, "enemies");
        mockBatch = mock(SpriteBatch.class);
    }

    @Test
    void testEnemyMovement() {
        when(gameView.getGameState()).thenReturn(GameState.PLAYING);
        assertEquals(enemyArray.size, 1);
        assertEquals(enemyArray.get(0).getLinearVelocity(), new Vector2(0.0f, 0.0f));
        enemy.enemyMovement();
        Body enemyBody1 = enemyArray.get(0);
        assertEquals(enemyBody1.getLinearVelocity(), new Vector2(-15, -enemyBody1.getWorldCenter().y));
    }

    @Test
    void testRemoveEnemy() {
        assertEquals(enemyArray.size, 1);
        assertTrue(!enemyArray.isEmpty());
        enemy.removeEnemy(enemyArray.get(0), world);
        assertTrue(enemyArray.isEmpty());
    }

    @Test
    void testUpdateEnemyArray() {
        assertEquals(enemyArray.size, 1);
        Body enemyBody = enemyArray.get(0);
        enemyBody.setTransform(new Vector2(-1, 0), 0);
        enemy.updateEnemyArray(enemyArray);
        assertTrue(enemyArray.isEmpty());
    }

    @Test
    void testEnemyRender() {
        enemy.render(enemyArray, mockBatch);
        verify(mockBatch, atLeastOnce()).draw(any(TextureRegion.class), anyFloat(), anyFloat(), anyFloat(), anyFloat());
    }

    @Test
    void testRenderDeathAnimation() {
        enemy.renderDeathAnimation(mockBatch);
        verify(mockBatch).draw((TextureRegion) any(), anyFloat(), anyFloat(), anyFloat(), anyFloat());
    }
}
