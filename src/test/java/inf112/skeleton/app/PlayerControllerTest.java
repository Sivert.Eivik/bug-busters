package inf112.skeleton.app;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;

import inf112.skeleton.app.model.Player;
import inf112.skeleton.app.model.PlayerState;
import inf112.skeleton.app.view.GameView;
import inf112.skeleton.app.controller.PlayerController;
import inf112.skeleton.app.model.Box2DPlayer;
import inf112.skeleton.app.model.Fireball;
import inf112.skeleton.app.model.GameState;
import inf112.skeleton.app.model.MenuScreen;
import inf112.skeleton.app.model.PauseButtons;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.backends.headless.HeadlessApplication;
import com.badlogic.gdx.backends.headless.HeadlessApplicationConfiguration;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.physics.box2d.WorldDef;

public class PlayerControllerTest {
    private Body mockedBody, enemyBody, playerBody, bodyB;
    private GameView mockedGameView;
    private Player mockedPlayer;
    private PlayerController controller;
    private Contact mockedContact;
    private Fixture mockedFixtureA, mockedFixtureB;
    private Fireball mockedFireball;
    private MenuScreen mockedMenuScreen;
    private PauseButtons mockedPauseButtons;
    private PlayerState playerstate;
    private Box2DPlayer box2dPlayer;
    private World world;
    private Rectangle playButtonRectangle;
    private Rectangle controlButtonRectangle;
    private Rectangle exitButtonRectangle;
    private Rectangle restartButtonRectangle;
    private Rectangle continueButtonRectangle;
    private Rectangle pauseExitButtonRectangle;
    private float screenWidth = 640;
    private float screenHeight = 320;

    @BeforeEach
    void setUp() throws Exception {
        HeadlessApplicationConfiguration cfg = new HeadlessApplicationConfiguration();
        ApplicationListener applicationListener = new ApplicationAdapter() {
        };
        Gdx.gl20 = mock(GL20.class);
        Gdx.gl = mock(GL20.class);
        new HeadlessApplication(applicationListener, cfg);
        world = new World(new WorldDef(new Vector2(0, 0), true));
        Gdx.gl20 = mock(GL20.class);
        Gdx.gl = mock(GL20.class);

        initializeMocks();
        configureMocks();
        mockedPlayer = new Player(playerstate);
        box2dPlayer = new Box2DPlayer(world);
        mockedBody = box2dPlayer.getBody();

        controller = new PlayerController(mockedBody, mockedPlayer, mockedGameView, world);
        setupReflectionFields();

        playButtonRectangle = new Rectangle((screenWidth / 2) - 100, (screenHeight / 2) + 50, 200, 100);
        controlButtonRectangle = new Rectangle((screenWidth / 2) - 100,
                playButtonRectangle.y - playButtonRectangle.height, 200, 100);
        restartButtonRectangle = new Rectangle((screenWidth / 2) - 70, (screenHeight / 2) - 55, 140, 100);
        continueButtonRectangle = new Rectangle(restartButtonRectangle.x - 100, restartButtonRectangle.y, 140, 100);
        exitButtonRectangle = new Rectangle(restartButtonRectangle.x + 100, restartButtonRectangle.y, 140, 100);
        pauseExitButtonRectangle = new Rectangle(restartButtonRectangle.x + 100, restartButtonRectangle.y, 140, 100);
    }

    private void initializeMocks() {
        mockedBody = mock(Body.class);
        mockedGameView = mock(GameView.class);
        mockedPlayer = mock(Player.class);
        mockedContact = mock(Contact.class);
        enemyBody = mock(Body.class);
        playerBody = mock(Body.class);
        mockedFixtureA = mock(Fixture.class);
        mockedFixtureB = mock(Fixture.class);
        bodyB = mock(Body.class);
        mockedFireball = mock(Fireball.class);
        mockedMenuScreen = mock(MenuScreen.class);
        mockedPauseButtons = mock(PauseButtons.class);
    }

    private void configureMocks() {
        when(mockedBody.getUserData()).thenReturn("player");
        when(enemyBody.getUserData()).thenReturn("enemies");
        when(playerBody.getUserData()).thenReturn("player");
        when(mockedContact.getFixtureA()).thenReturn(mockedFixtureA);
        when(mockedContact.getFixtureB()).thenReturn(mockedFixtureB);
        when(mockedFixtureA.getBody()).thenReturn(playerBody);
        when(mockedFixtureB.getBody()).thenReturn(enemyBody);
        when(mockedBody.getPosition()).thenReturn(new Vector2(10, 5));
        when(mockedBody.getMass()).thenReturn(10f);
        when(mockedBody.getLinearVelocity()).thenReturn(new Vector2(0, 0));
    }

    private void setupReflectionFields() throws NoSuchFieldException, IllegalAccessException {
        setPrivateField(controller, "ableToJump", true);
        setPrivateField(controller, "groundContacts", 1);
        setPrivateField(controller, "body", mockedBody);
        setPrivateField(controller, "movingRight", false);
        setPrivateField(controller, "movingLeft", false);
        setPrivateField(controller, "gameView", mockedGameView);
        setPrivateField(controller, "menuScreen", mockedMenuScreen);
        setPrivateField(controller, "pauseButtons", mockedPauseButtons);
        setPrivateField(controller, "fireball", mockedFireball);
    }

    private void setPrivateField(Object targetObject, String fieldName, Object valueToSet)
            throws NoSuchFieldException, IllegalAccessException {
        java.lang.reflect.Field field = targetObject.getClass().getDeclaredField(fieldName);
        field.setAccessible(true);
        field.set(targetObject, valueToSet);
    }

    @Test
    void testBeginContactWithEnemy() throws Exception {

        java.lang.reflect.Field groundContactsField = PlayerController.class.getDeclaredField("groundContacts");
        groundContactsField.setAccessible(true);

        // Bekrefte at groundContacts er satt korrekt før kallet
        assertTrue((int) groundContactsField.get(controller) > 0, "groundContacts should be more than 0");

        controller.beginContact(mockedContact);
        verify(mockedGameView).setGameState(GameState.GAME_OVER);
    }

    /*
     * This test belongs to testBeginContactWithEnemy() to show that the
     * intreactions with GameView acutually happen. This is done to find out the
     * issue with testBeginContactWithEnemy()
     */
    @Test
    void testGameViewSetState() {
        mockedGameView.setGameState(GameState.GAME_OVER);
        verify(mockedGameView).setGameState(GameState.GAME_OVER);
    }

    // Test below are for fromAirToStandingStill()
    @Test
    void whenVelocityIsZeroAndFacingRight_TransitionToStandingRight() {
        mockedBody.setLinearVelocity(0, 0);
        mockedPlayer.setPlayerState(PlayerState.RUNNING_RIGHT);
        controller.fromAirToStandingStill();
        assertEquals(PlayerState.STANDING_RIGHT, mockedPlayer.getPlayerState());

        mockedPlayer.setPlayerState(PlayerState.RUNNING_LEFT);
        controller.fromAirToStandingStill();
        assertEquals(PlayerState.STANDING_LEFT, mockedPlayer.getPlayerState());
    }

    @Test
    void testPlayerEndsContactWithGround() throws Exception {
        when(mockedFixtureA.getBody()).thenReturn(playerBody);
        when(mockedFixtureB.getBody()).thenReturn(bodyB);
        when(playerBody.getUserData()).thenReturn("player");
        when(bodyB.getUserData()).thenReturn("ground");

        // Reflection to set groundContacts to 1 before the contacts ends
        java.lang.reflect.Field groundContactsField = PlayerController.class.getDeclaredField("groundContacts");
        groundContactsField.setAccessible(true);
        groundContactsField.setInt(controller, 1);

        controller.endContact(mockedContact);

        assertEquals(0, groundContactsField.getInt(controller), "groundContacts should be zero after contact ends");
        assertFalse(controller.ableToJump, "ableToJump should be false when there are no ground contacts");

    }

    @Test
    void testGetFireball() throws Exception {
        java.lang.reflect.Field fireballField = PlayerController.class.getDeclaredField("fireball");
        fireballField.setAccessible(true);
        fireballField.set(controller, mockedFireball);

        Fireball actualFireball = controller.getFireball();
        assertEquals(mockedFireball, actualFireball,
                "The returned fireball should be the same as the one set in controller.");
    }

    @Test
    void testTouchUp() {
        boolean result = controller.touchUp(100, 200, 1, 0);
        assertFalse(result, "touchUp should return false.");
    }

    @Test
    void testTouchCancelled() {
        boolean result = controller.touchCancelled(100, 200, 1, 0);
        assertFalse(result, "touchCancelled should return false.");
    }

    @Test
    void testTouchDragged() {
        boolean result = controller.touchDragged(100, 200, 1);
        assertFalse(result, "touchDragged should return false.");
    }

    @Test
    void testKeyDownForShift() throws NoSuchFieldException, IllegalAccessException {
        java.lang.reflect.Field fireBallArray = Fireball.class.getDeclaredField("fireBallArray");
        fireBallArray.setAccessible(true);
        Array<Body> mockFireballArray = new Array<>();
        mockFireballArray.size = 0;
        fireBallArray.set(mockedFireball, mockFireballArray);

        mockedPlayer.setPlayerState(PlayerState.STANDING_RIGHT);
        controller.keyDown(Input.Keys.SHIFT_LEFT);
        assertEquals(PlayerState.SHOOTING_RIGHT, mockedPlayer.getPlayerState());

    }

    @Test
    void testKeyDownForShift2() throws NoSuchFieldException, IllegalAccessException {
        java.lang.reflect.Field fireBallArray = Fireball.class.getDeclaredField("fireBallArray");
        fireBallArray.setAccessible(true);
        Array<Body> mockFireballArray = new Array<>();
        mockFireballArray.size = 0;
        fireBallArray.set(mockedFireball, mockFireballArray);

        mockedPlayer.setPlayerState(PlayerState.JUMPING_RIGHT);
        controller.keyDown(Input.Keys.SHIFT_LEFT);
        assertEquals(PlayerState.SHOOTING_MID_AIR_RIGHT, mockedPlayer.getPlayerState());

    }

    @Test
    void testKeyDownForShift3() throws NoSuchFieldException, IllegalAccessException {
        java.lang.reflect.Field fireBallArray = Fireball.class.getDeclaredField("fireBallArray");
        fireBallArray.setAccessible(true);
        Array<Body> mockFireballArray = new Array<>();
        mockFireballArray.size = 0;
        fireBallArray.set(mockedFireball, mockFireballArray);

        mockedPlayer.setPlayerState(PlayerState.STANDING_LEFT);
        controller.keyDown(Input.Keys.SHIFT_LEFT);
        assertEquals(PlayerState.SHOOTING_LEFT, mockedPlayer.getPlayerState());
    }

    @Test
    void testKeyDownForShift4() throws NoSuchFieldException, IllegalAccessException {
        java.lang.reflect.Field fireBallArray = Fireball.class.getDeclaredField("fireBallArray");
        fireBallArray.setAccessible(true);
        Array<Body> mockFireballArray = new Array<>();
        mockFireballArray.size = 0;
        fireBallArray.set(mockedFireball, mockFireballArray);

        mockedPlayer.setPlayerState(PlayerState.JUMPING_LEFT);
        controller.keyDown(Input.Keys.SHIFT_LEFT);
        assertEquals(PlayerState.SHOOTING_MID_AIR_LEFT, mockedPlayer.getPlayerState());

    }

    @Test
    void testEscapeKeyUnderControls() {
        when(mockedGameView.getGameState()).thenReturn(GameState.CONTROLS);
        controller.keyDown(Input.Keys.ESCAPE);
        verify(mockedGameView).setGameState(GameState.WELCOME);
    }

    @Test
    void testKeyDownDMovesPlayerRight() {
        assertEquals(mockedBody.getLinearVelocity().x, 0);
        controller.keyDown(Input.Keys.D);
        assertTrue(mockedBody.getLinearVelocity().x > 0);
    }

    @Test
    void testKeyDownAMovesPlayerLeft() {
        assertEquals(mockedBody.getLinearVelocity().x, 0);
        controller.keyDown(Input.Keys.A);
        assertTrue(mockedBody.getLinearVelocity().x < 0);
    }

    @Test
    void testKeyUpDStopsPlayerMoving() {
        controller.keyDown(Input.Keys.D);
        assertTrue(mockedBody.getLinearVelocity().x > 0);
        controller.keyUp(Input.Keys.D);
        assertEquals(mockedBody.getLinearVelocity().x, 0);
    }

    @Test
    void testKeyUpAStopsPlayerMovingLeft() {
        controller.keyDown(Input.Keys.A);
        assertTrue(mockedBody.getLinearVelocity().x < 0);
        controller.keyUp(Input.Keys.A);
        assertEquals(mockedBody.getLinearVelocity().x, 0);
    }

    @Test
    void testJumpingStateWhenAbleToJump() {
        mockedBody.setLinearVelocity(10, 0);
        controller.keyDown(Input.Keys.SPACE);
        assertEquals(PlayerState.JUMPING_RIGHT, mockedPlayer.getPlayerState());
    }

    @Test
    void testJumpingAndMovingRight() {
        assertEquals(mockedBody.getLinearVelocity().y, 0);
        assertEquals(mockedBody.getLinearVelocity().x, 0);
        controller.keyDown(Input.Keys.D);
        controller.keyDown(Input.Keys.SPACE);
        assertNotEquals(mockedBody.getLinearVelocity().x, 0);
        assertNotEquals(mockedBody.getLinearVelocity().y, 0);
    }

    @Test
    void testJumpingAndMovingLeft() {
        assertEquals(mockedBody.getLinearVelocity().x, 0);
        assertEquals(mockedBody.getLinearVelocity().y, 0);
        controller.keyDown(Input.Keys.A);
        controller.keyDown(Input.Keys.SPACE);
        assertNotEquals(mockedBody.getLinearVelocity().x, 0);
        assertNotEquals(mockedBody.getLinearVelocity().y, 0);

    }

    @Test
    void testBeginContactWithEnemyOnGround() throws NoSuchFieldException, IllegalAccessException {
        when(mockedFixtureA.getBody()).thenReturn(playerBody);
        when(mockedFixtureB.getBody()).thenReturn(enemyBody);
        when(playerBody.getUserData()).thenReturn("player");
        when(enemyBody.getUserData()).thenReturn("enemies");
        setPrivateField(controller, "groundContacts", 1);
    }

    @Test
    void testBeginContactWithEnemyInAir() throws Exception {
        setPrivateField(controller, "groundContacts", 0);

        Vector2 expectedImpulse = new Vector2(0.0f, mockedBody.getMass() * 120);
        Vector2 expectedPoint = mockedBody.getWorldCenter();

        controller.beginContact(mockedContact);

        verify(playerBody).applyLinearImpulse(eq(expectedImpulse), eq(expectedPoint),
                eq(true));
    }

    @Test
    void testTouchDownWelcomeStatePlayButton2() {
        mockedMenuScreen.playButtonRectangle = playButtonRectangle;
        when(mockedGameView.getGameState()).thenReturn(GameState.WELCOME);
        when(mockedMenuScreen.isButtonPressed(mockedMenuScreen.playButtonRectangle)).thenReturn(true);

        controller.touchDown((int) mockedMenuScreen.playButtonRectangle.x, (int) mockedMenuScreen.playButtonRectangle.y,
                0, 0);
        assertTrue(mockedMenuScreen.isButtonPressed(mockedMenuScreen.playButtonRectangle));
    }

    @Test
    void testTouchDownWelcomeStateExitButton() {
        mockedMenuScreen.exitButtonRectangle = exitButtonRectangle;
        when(mockedGameView.getGameState()).thenReturn(GameState.WELCOME);
        when(mockedMenuScreen.isButtonPressed(mockedMenuScreen.exitButtonRectangle)).thenReturn(true);

        controller.touchDown((int) mockedMenuScreen.exitButtonRectangle.x, (int) mockedMenuScreen.exitButtonRectangle.y,
                0, 0);

        assertTrue(mockedMenuScreen.isButtonPressed(mockedMenuScreen.exitButtonRectangle));
    }

    @Test
    void testTouchDownWelcomeStateControlsButton() {
        mockedMenuScreen.controlButtonRectangle = controlButtonRectangle;
        when(mockedGameView.getGameState()).thenReturn(GameState.WELCOME);
        when(mockedMenuScreen.isButtonPressed(mockedMenuScreen.controlButtonRectangle)).thenReturn(true);

        controller.touchDown((int) mockedMenuScreen.controlButtonRectangle.x,
                (int) mockedMenuScreen.controlButtonRectangle.y, 0, 0);

        assertTrue(mockedMenuScreen.isButtonPressed(mockedMenuScreen.controlButtonRectangle));
    }

    @Test
    void testTouchDownPausedStateContinueButton() {
        mockedPauseButtons.continueButtonRectangle = continueButtonRectangle;
        when(mockedGameView.getGameState()).thenReturn(GameState.PAUSED);

        when(mockedMenuScreen.isButtonPressed(mockedPauseButtons.continueButtonRectangle)).thenReturn(true);

        controller.touchDown((int) mockedPauseButtons.continueButtonRectangle.x,
                (int) mockedPauseButtons.continueButtonRectangle.y, 0, 0);

        assertTrue(mockedMenuScreen.isButtonPressed(mockedPauseButtons.continueButtonRectangle));
    }

    @Test
    void testTouchDownPausedStateRestartButton() {
        mockedPauseButtons.restartButtonRectangle = restartButtonRectangle;
        when(mockedGameView.getGameState()).thenReturn(GameState.PAUSED);

        when(mockedMenuScreen.isButtonPressed(mockedPauseButtons.restartButtonRectangle)).thenReturn(true);

        controller.touchDown((int) mockedPauseButtons.restartButtonRectangle.x,
                (int) mockedPauseButtons.restartButtonRectangle.y, 0, 0);

        assertTrue(mockedMenuScreen.isButtonPressed(mockedPauseButtons.restartButtonRectangle));
    }

    @Test
    void testTouchDownPausedStateExitButton() {
        mockedPauseButtons.exitButtonRectangle = exitButtonRectangle;

        when(mockedGameView.getGameState()).thenReturn(GameState.PAUSED);

        when(mockedMenuScreen.isButtonPressed(mockedPauseButtons.exitButtonRectangle)).thenReturn(true);

        controller.touchDown((int) mockedPauseButtons.exitButtonRectangle.x,
                (int) mockedPauseButtons.exitButtonRectangle.y, 0, 0);

        assertTrue(mockedMenuScreen.isButtonPressed(mockedPauseButtons.exitButtonRectangle));
    }

    @Test
    void testTouchDownPlayingStatePauseButton() {
        when(mockedGameView.getGameState()).thenReturn(GameState.PLAYING);
        when(mockedMenuScreen.isButtonPressed(mockedPauseButtons.pauseButtonRectangle)).thenReturn(true);

        controller.touchDown(0, 0, 0, 0);

        verify(mockedGameView).setGameState(GameState.PAUSED);
    }

    // Test bellow are for mouseMoved()
    @Test
    void testMouseMovedPlayButtonWelcomeState() {
        mockedMenuScreen.playButtonRectangle = playButtonRectangle;
        when(mockedGameView.getGameState()).thenReturn(GameState.WELCOME);

        when(mockedMenuScreen.isMouseOverButton(mockedMenuScreen.playButtonRectangle)).thenReturn(true);

        controller.mouseMoved((int) mockedMenuScreen.playButtonRectangle.x,
                (int) mockedMenuScreen.playButtonRectangle.y);

        verify(mockedMenuScreen).setButtonState(mockedMenuScreen.playButtonRectangle, true);
        verify(mockedMenuScreen, times(2)).setButtonState(mockedMenuScreen.exitButtonRectangle, false);
        verify(mockedMenuScreen, times(2)).setButtonState(mockedMenuScreen.controlButtonRectangle, false);
    }

    @Test
    void testMouseMovedControlsButtonWelcomeState() {
        mockedMenuScreen.controlButtonRectangle = controlButtonRectangle;

        when(mockedGameView.getGameState()).thenReturn(GameState.WELCOME);

        when(mockedMenuScreen.isMouseOverButton(mockedMenuScreen.controlButtonRectangle)).thenReturn(true);

        controller.mouseMoved((int) mockedMenuScreen.controlButtonRectangle.x,
                (int) mockedMenuScreen.controlButtonRectangle.y);

        verify(mockedMenuScreen).setButtonState(mockedMenuScreen.controlButtonRectangle, true);
        verify(mockedMenuScreen, times(2)).setButtonState(mockedMenuScreen.exitButtonRectangle, false);
        verify(mockedMenuScreen, times(2)).setButtonState(mockedMenuScreen.playButtonRectangle, false);
    }

    @Test
    void testMouseMovedExitButtonWelcomeState() {
        mockedMenuScreen.exitButtonRectangle = exitButtonRectangle;

        when(mockedGameView.getGameState()).thenReturn(GameState.WELCOME);

        when(mockedMenuScreen.isMouseOverButton(mockedMenuScreen.exitButtonRectangle)).thenReturn(true);

        controller.mouseMoved((int) mockedMenuScreen.exitButtonRectangle.x,
                (int) mockedMenuScreen.exitButtonRectangle.y);

        verify(mockedMenuScreen).setButtonState(mockedMenuScreen.exitButtonRectangle, true);
        verify(mockedMenuScreen, times(2)).setButtonState(mockedMenuScreen.controlButtonRectangle, false);
        verify(mockedMenuScreen, times(2)).setButtonState(mockedMenuScreen.playButtonRectangle, false);
    }

    @Test
    void testMouseMovedContinueButtonPausedState() {
        mockedPauseButtons.continueButtonRectangle = continueButtonRectangle;
        when(mockedGameView.getGameState()).thenReturn(GameState.PAUSED);
        when(mockedMenuScreen.isMouseOverButton(mockedPauseButtons.continueButtonRectangle)).thenReturn(true);

        controller.mouseMoved((int) mockedPauseButtons.continueButtonRectangle.x,
                (int) mockedPauseButtons.continueButtonRectangle.y);

        verify(mockedPauseButtons).pauseButtonState(mockedPauseButtons.continueButtonRectangle,
                true);
        verify(mockedPauseButtons,
                times(2)).pauseButtonState(mockedPauseButtons.restartButtonRectangle, false);
        verify(mockedPauseButtons,
                times(2)).pauseButtonState(mockedPauseButtons.exitButtonRectangle, false);
    }

    @Test
    void testMouseMovedRestartButtonPausedState() {
        mockedPauseButtons.restartButtonRectangle = restartButtonRectangle;
        when(mockedGameView.getGameState()).thenReturn(GameState.PAUSED);
        when(mockedMenuScreen.isMouseOverButton(mockedPauseButtons.restartButtonRectangle)).thenReturn(true);

        controller.mouseMoved((int) mockedPauseButtons.restartButtonRectangle.x,
                (int) mockedPauseButtons.restartButtonRectangle.y);

        verify(mockedPauseButtons).pauseButtonState(mockedPauseButtons.restartButtonRectangle,
                true);
        verify(mockedPauseButtons,
                times(2)).pauseButtonState(mockedPauseButtons.continueButtonRectangle, false);
        verify(mockedPauseButtons,
                times(2)).pauseButtonState(mockedPauseButtons.exitButtonRectangle, false);
    }

    @Test
    void testMouseMovedExitButtonPausedState() {
        mockedPauseButtons.exitButtonRectangle = pauseExitButtonRectangle;
        when(mockedGameView.getGameState()).thenReturn(GameState.PAUSED);
        when(mockedMenuScreen.isMouseOverButton(mockedPauseButtons.exitButtonRectangle)).thenReturn(true);

        controller.mouseMoved((int) mockedPauseButtons.exitButtonRectangle.x,
                (int) mockedPauseButtons.exitButtonRectangle.y);

        verify(mockedPauseButtons).pauseButtonState(mockedPauseButtons.exitButtonRectangle,
                true);
        verify(mockedPauseButtons,
                times(2)).pauseButtonState(mockedPauseButtons.restartButtonRectangle, false);
        verify(mockedPauseButtons,
                times(2)).pauseButtonState(mockedPauseButtons.continueButtonRectangle, false);
    }

    @Test
    void testMouseMovedNoButtonHoveredInPausedState() {
        when(mockedGameView.getGameState()).thenReturn(GameState.PAUSED);
        when(mockedMenuScreen.isMouseOverButton(any())).thenReturn(false);

        controller.mouseMoved(0, 0);

        verify(mockedPauseButtons, times(3)).pauseButtonState(mockedPauseButtons.continueButtonRectangle, false);
        verify(mockedPauseButtons, times(3)).pauseButtonState(mockedPauseButtons.exitButtonRectangle, false);
        verify(mockedPauseButtons, times(3)).pauseButtonState(mockedPauseButtons.restartButtonRectangle, false);
    }

}
