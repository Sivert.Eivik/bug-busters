package inf112.skeleton.app;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Files;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Graphics;
import com.badlogic.gdx.backends.headless.HeadlessApplication;
import com.badlogic.gdx.backends.headless.HeadlessApplicationConfiguration;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Stage;

import inf112.skeleton.app.model.MenuScreen;
import inf112.skeleton.app.model.PauseButtons;

import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.files.FileHandle;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockedConstruction;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyFloat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class PauseButtonTest {
    @Mock
    private MenuScreen menuScreen;
    @Mock
    private Texture pauseMenuTexture, pauseButtonsTexture;
    @Mock
    private TextureRegion mockTextureRegion;
    @Mock
    private Stage stage;

    HeadlessApplicationConfiguration config;

    static MockedConstruction<Texture> texture;

    private PauseButtons pauseButtons;

    @BeforeEach
    void setUp() {
        config = new HeadlessApplicationConfiguration();
        ApplicationListener listener = new ApplicationAdapter() {
        };

        Gdx.gl = mock(GL20.class);
        Gdx.graphics = mock(Graphics.class);
        Gdx.files = mock(Files.class);
        new HeadlessApplication(listener, config);
        pauseButtons = new PauseButtons(menuScreen);

        FileHandle fileHandle = mock(FileHandle.class);
        when(fileHandle.name()).thenReturn("mock.png");

        pauseButtons = new PauseButtons(menuScreen);
    }

    @Test
    void testDrawPauseButton() {
        SpriteBatch spriteBatch = mock(SpriteBatch.class);
        OrthographicCamera camera = new OrthographicCamera();
        pauseButtons.drawPauseButton(spriteBatch, camera);
        verify(spriteBatch).draw(any(TextureRegion.class), anyFloat(), anyFloat(), anyFloat(), anyFloat());
    }

    @Test
    void testButtonStateChange() {
        Rectangle button = pauseButtons.continueButtonRectangle;
        pauseButtons.pauseButtonState(button, true);
        assertTrue(pauseButtons.continueDown);
    }

    @Test
    void testPauseMenuDrawing() {
        SpriteBatch spriteBatch = mock(SpriteBatch.class);
        Vector3 cameraPosition = new Vector3(400, 300, 0);
        pauseButtons.drawPauseMenu(spriteBatch, cameraPosition);

        verify(spriteBatch).draw(any(Texture.class), eq(200.0f), eq(100.0f), eq(400.0f), eq(400.0f)); // Pause menu
        verify(spriteBatch).draw(any(TextureRegion.class), eq(225.0f), eq(-55.0f), eq(140.0f), eq(100.0f)); // Continue
                                                                                                            // button
        verify(spriteBatch).draw(any(TextureRegion.class), eq(330.0f), eq(-55.0f), eq(140.0f), eq(100.0f)); // Restart
                                                                                                            // button
        verify(spriteBatch).draw(any(TextureRegion.class), eq(435.0f), eq(-55.0f), eq(140.0f), eq(100.0f)); // Exit
                                                                                                            // button
    }

}
