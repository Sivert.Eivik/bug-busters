package inf112.skeleton.app;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.headless.HeadlessApplication;
import com.badlogic.gdx.backends.headless.HeadlessApplicationConfiguration;
import com.badlogic.gdx.graphics.GL20;

import inf112.skeleton.app.model.GameState;
import inf112.skeleton.app.model.MarioModel;
import inf112.skeleton.app.view.GameView;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

public class GameViewTest {
    MarioModel model;
    GameView gameView;

    @BeforeEach
    void setUp() {
        HeadlessApplicationConfiguration cfg = new HeadlessApplicationConfiguration();
        ApplicationListener applicationListener = new ApplicationAdapter() {
        };
        Gdx.gl20 = mock(GL20.class);
        Gdx.gl = mock(GL20.class);
        new HeadlessApplication(applicationListener, cfg);
        model = mock(MarioModel.class);
        gameView = new GameView(model);
    }

    @Test
    void testSetCurrentGameState() {
        assertEquals(gameView.getGameState(), GameState.WELCOME);
        gameView.setGameState(GameState.PLAYING);
        assertEquals(gameView.getGameState(), GameState.PLAYING);
        gameView.setGameState(GameState.GAME_OVER);
        assertEquals(gameView.getGameState(), GameState.GAME_OVER);
        gameView.setGameState(GameState.WINNER);
        assertEquals(gameView.getGameState(), GameState.WINNER);
        gameView.setGameState(GameState.PAUSED);
        assertEquals(gameView.getGameState(), GameState.PAUSED);
        gameView.setGameState(GameState.EXIT);
        assertEquals(gameView.getGameState(), GameState.EXIT);
    }

}
