package inf112.skeleton.app;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.backends.headless.HeadlessApplication;
import com.badlogic.gdx.backends.headless.HeadlessApplicationConfiguration;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.WorldDef;

import inf112.skeleton.app.controller.PlayerController;
import inf112.skeleton.app.levels.LevelHandler;
import inf112.skeleton.app.model.Box2DPlayer;
import inf112.skeleton.app.model.Enemy;
import inf112.skeleton.app.model.MenuScreen;
import inf112.skeleton.app.model.Player;
import inf112.skeleton.app.view.GameView;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyFloat;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class MenuScreenTest {
    Body mockBody;
    Player mockPlayer;
    GameView mockGameView;
    MenuScreen menuScreen;
    PlayerController mockPlayerController;
    Rectangle mockRectangle;
    SpriteBatch mockBatch;
    LevelHandler mockLevelHandler;
    Enemy enemy;
    Box2DPlayer box2DPlayer;

    @BeforeEach
    void setUp() {
        HeadlessApplicationConfiguration cfg = new HeadlessApplicationConfiguration();
        ApplicationListener applicationListener = new ApplicationAdapter() {
        };
        new HeadlessApplication(applicationListener, cfg);
        Gdx.gl20 = mock(GL20.class);
        Gdx.gl = mock(GL20.class);
        Gdx.input = mock(Input.class);
        mockBatch = mock(SpriteBatch.class);
        mockBody = mock(Body.class);
        mockPlayer = mock(Player.class);
        mockGameView = mock(GameView.class);
        menuScreen = mock(MenuScreen.class);
        mockRectangle = mock(Rectangle.class);
        enemy = mock(Enemy.class);
        box2DPlayer = mock(Box2DPlayer.class);
        mockLevelHandler = new LevelHandler(new World(new WorldDef(new Vector2(0, -9.81f), false)), box2DPlayer, enemy,
                mockPlayer,
                mockLevelHandler);
        menuScreen = new MenuScreen(mockBatch);
    }

    @Test
    void testDrawControllScreen() {
        menuScreen.drawControlsScreen(mockBatch, mockLevelHandler);
        verify(mockBatch, atLeastOnce()).draw((Texture) any(), anyFloat(), anyFloat(), anyFloat(), anyFloat());
    }

    @Test
    void testDrawWelcomeScreen() {
        menuScreen.drawWelcomeScreen(mockBatch, mockLevelHandler);
        verify(mockBatch, atLeastOnce()).draw((Texture) any(), anyFloat(), anyFloat(), anyFloat(), anyFloat());
    }

    @Test
    void testSetButtonStage() throws NoSuchFieldException, IllegalAccessException {
        java.lang.reflect.Field playDownField = MenuScreen.class.getDeclaredField("playButtonDown");
        playDownField.setAccessible(true);

        Rectangle button = menuScreen.playButtonRectangle;
        menuScreen.setButtonState(button, true);

        boolean playDownValue = (Boolean) playDownField.get(menuScreen);

        assertTrue(playDownValue);
    }
}
