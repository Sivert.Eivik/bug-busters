package inf112.skeleton.app;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.headless.HeadlessApplication;
import com.badlogic.gdx.backends.headless.HeadlessApplicationConfiguration;
import com.badlogic.gdx.graphics.GL20;
import inf112.skeleton.app.map.MapCollision;
import org.junit.jupiter.api.BeforeEach;

import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.WorldDef;

import inf112.skeleton.app.levels.Level1;
import inf112.skeleton.app.levels.Level2;
import inf112.skeleton.app.model.Box2DPlayer;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;

import java.util.List;

public class MapCollisionTest {
    private Box2DPlayer box2DPlayer;
    private TiledMap map;
    private World world;
    private MapCollision mapCollision;
    private Level1 level1;
    private Level2 level2;

    @BeforeEach
    void setUp() {
        HeadlessApplicationConfiguration cfg = new HeadlessApplicationConfiguration();
        ApplicationListener applicationListener = new ApplicationAdapter() {
        };
        new HeadlessApplication(applicationListener, cfg);
        Gdx.gl20 = mock(GL20.class);
        Gdx.gl = mock(GL20.class);
        world = new World(new WorldDef(new Vector2(0, -9.81f), true));
        box2DPlayer = new Box2DPlayer(world);
        mapCollision = new MapCollision();
        level1 = new Level1();
        level2 = new Level2();
    }

    @Test
    void testFinishCollision() {
        map = level1.map();
        float finishPositionXMap1 = 59;
        float finishPositionYMap1 = 2;
        box2DPlayer.getBody().setTransform(finishPositionXMap1, finishPositionYMap1, 0);
        List<Float> finishPosition = mapCollision.updateFinishCollision(map);
        assertEquals(finishPosition.get(0), finishPositionXMap1);
        assertEquals(finishPosition.get(1), finishPositionYMap1);
        box2DPlayer.getBody().setTransform(new Vector2(finishPositionXMap1 * 32, finishPositionYMap1 * 32),
                0);
        assertTrue(mapCollision.finishCollision(box2DPlayer, map));
        map = level2.map();

        float finishPositionXMap2 = 48;
        float finishPositionYMap2 = 4;
        finishPosition = mapCollision.updateFinishCollision(map);
        assertEquals(finishPosition.get(0), finishPositionXMap2);
        assertEquals(finishPosition.get(1), finishPositionYMap2);
        box2DPlayer.getBody().setTransform(new Vector2(finishPositionXMap2 * 32, finishPositionYMap2 * 32), 0);
        assertTrue(mapCollision.finishCollision(box2DPlayer, map));
    }

    @Test
    void testUpdateFinishPosition() {
        map = level1.map();
        List<Float> finishPosition1 = mapCollision.updateFinishCollision(map);
        map = level2.map();
        List<Float> finishPosition2 = mapCollision.updateFinishCollision(map);
        assertNotEquals(finishPosition1, finishPosition2);
    }
}
