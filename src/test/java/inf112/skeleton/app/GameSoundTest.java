package inf112.skeleton.app;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.badlogic.gdx.Audio;
import com.badlogic.gdx.Files;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.files.FileHandle;

import inf112.skeleton.app.model.GameSounds;
import inf112.skeleton.app.model.GameState;
import inf112.skeleton.app.view.GameView;

public class GameSoundTest {
    @Mock
    private GameView gameView;
    @Mock
    private Music backgroundMusic, gameOverMusic, victoryMusic;
    @Mock
    private Audio audio;
    @Mock
    private FileHandle fileHandle;

    private GameSounds gameSounds;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        when(audio.newMusic(any(FileHandle.class))).thenReturn(backgroundMusic, gameOverMusic, victoryMusic);
        Gdx.audio = audio;
        Gdx.files = mock(Files.class);
        when(Gdx.files.internal("background.mp3")).thenReturn(fileHandle);
        when(Gdx.files.internal("gameover.mp3")).thenReturn(fileHandle);
        when(Gdx.files.internal("victory.mp3")).thenReturn(fileHandle);
        gameSounds = new GameSounds(gameView);
    }

    @Test
    void testPlayMusic() {
         when(gameView.getGameState()).thenReturn(GameState.PLAYING);
         gameSounds.playMusic();
         verify(backgroundMusic).play();

         when(gameView.getGameState()).thenReturn(GameState.GAME_OVER);
         gameSounds.playMusic();
         verify(gameOverMusic).play();

         when(gameView.getGameState()).thenReturn(GameState.WINNER);
         gameSounds.playMusic();
         verify(victoryMusic).play();
    }
}
