package inf112.skeleton.app;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyFloat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Graphics;
import com.badlogic.gdx.backends.headless.HeadlessApplication;
import com.badlogic.gdx.backends.headless.HeadlessApplicationConfiguration;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.WorldDef;

import inf112.skeleton.app.model.Box2DPlayer;
import inf112.skeleton.app.model.Player;
import inf112.skeleton.app.model.PlayerState;

public class PlayerTest {
    private Player mockedPlayer;
    private Box2DPlayer mockedBody;
    private PlayerState playerState;
    private SpriteBatch mockedBatch;
    World world;

    @BeforeEach
    void setUp() throws Exception {
        HeadlessApplicationConfiguration cfg = new HeadlessApplicationConfiguration();
        ApplicationListener applicationListener = new ApplicationAdapter() {
        };
        Gdx.gl20 = mock(GL20.class);
        Gdx.gl = mock(GL20.class);
        new HeadlessApplication(applicationListener, cfg);
        world = new World(new WorldDef(new Vector2(0, 0), true));
        Gdx.gl20 = mock(GL20.class);
        Gdx.gl = mock(GL20.class);

        mockedPlayer = mock(Player.class);
        mockedBody = mock(Box2DPlayer.class);
        mockedBatch = mock(SpriteBatch.class);
        Gdx.graphics = mock(Graphics.class);

        playerState = PlayerState.STANDING_RIGHT;
        mockedPlayer = new Player(playerState);
        mockedBody = new Box2DPlayer(world);
    }

    @Test
    void testGetPlayerState() {
        assertEquals(PlayerState.STANDING_RIGHT, mockedPlayer.getPlayerState());
        assertNotEquals(PlayerState.SHOOTING_RIGHT, mockedPlayer.getPlayerState());

    }

    @Test
    void testSetPlayerState() {
        mockedPlayer.setPlayerState(PlayerState.RUNNING_RIGHT);
        assertEquals(PlayerState.RUNNING_RIGHT, mockedPlayer.getPlayerState());

    }

    @Test
    void testRenderStandingRightAnimation() {
        mockedPlayer.renderPlayerAnimations(mockedBatch, mockedBody.getBody());
        verify(mockedBatch).draw((TextureRegion) any(), anyFloat(), anyFloat());
    }

    @Test
    void testRenderRunningRightAnimation() {
        mockedPlayer.setPlayerState(PlayerState.RUNNING_RIGHT);
        mockedPlayer.renderPlayerAnimations(mockedBatch, mockedBody.getBody());
        verify(mockedBatch).draw((TextureRegion) any(), anyFloat(), anyFloat());
    }

    @Test
    void testRenderRunningLeftAnimation() {
        mockedPlayer.setPlayerState(PlayerState.RUNNING_LEFT);
        mockedPlayer.renderPlayerAnimations(mockedBatch, mockedBody.getBody());
        verify(mockedBatch).draw((TextureRegion) any(), anyFloat(), anyFloat());
    }

    @Test
    void testJumpingRightAnimation() {
        mockedPlayer.setPlayerState(PlayerState.JUMPING_RIGHT);
        mockedPlayer.renderPlayerAnimations(mockedBatch, mockedBody.getBody());
        verify(mockedBatch).draw((TextureRegion) any(), anyFloat(), anyFloat());
    }

    @Test
    void testJumpingLeftAnimation() {
        mockedPlayer.setPlayerState(PlayerState.JUMPING_LEFT);
        mockedPlayer.renderPlayerAnimations(mockedBatch, mockedBody.getBody());
        verify(mockedBatch).draw((TextureRegion) any(), anyFloat(), anyFloat());
    }

    @Test
    void testShootingRightAnimation() {
        mockedPlayer.setPlayerState(PlayerState.SHOOTING_RIGHT);
        mockedPlayer.renderPlayerAnimations(mockedBatch, mockedBody.getBody());
        verify(mockedBatch).draw((TextureRegion) any(), anyFloat(), anyFloat());
    }

    @Test
    void testShootingLeftAnimation() {
        mockedPlayer.setPlayerState(PlayerState.SHOOTING_LEFT);
        mockedPlayer.renderPlayerAnimations(mockedBatch, mockedBody.getBody());
        verify(mockedBatch).draw((TextureRegion) any(), anyFloat(), anyFloat());
    }

    @Test
    void testShootingMidAirRight() {
        mockedPlayer.setPlayerState(PlayerState.SHOOTING_MID_AIR_RIGHT);
        mockedPlayer.renderPlayerAnimations(mockedBatch, mockedBody.getBody());
        verify(mockedBatch).draw((TextureRegion) any(), anyFloat(), anyFloat());
    }

    @Test
    void testShootingMidAirLeft() {
        mockedPlayer.setPlayerState(PlayerState.SHOOTING_MID_AIR_LEFT);
        mockedPlayer.renderPlayerAnimations(mockedBatch, mockedBody.getBody());
        verify(mockedBatch).draw((TextureRegion) any(), anyFloat(), anyFloat());
    }

    @Test
    void testStandingLeftAnimation() {
        mockedPlayer.setPlayerState(PlayerState.STANDING_LEFT);
        mockedPlayer.renderPlayerAnimations(mockedBatch, mockedBody.getBody());
        verify(mockedBatch).draw((TextureRegion) any(), anyFloat(), anyFloat());
    }
}
