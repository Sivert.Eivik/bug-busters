package inf112.skeleton.app;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.headless.HeadlessApplication;
import com.badlogic.gdx.backends.headless.HeadlessApplicationConfiguration;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.utils.Array;
import inf112.skeleton.app.levels.Level1;
import inf112.skeleton.app.model.MarioModel;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

public class MarioModelTest {
    private MarioModel model;
    private TiledMap map;
    private World world;
    private Level1 level1;

    @BeforeEach
    void setUp() {
        HeadlessApplicationConfiguration cfg = new HeadlessApplicationConfiguration();
        ApplicationListener applicationListener = new ApplicationAdapter() {
        };
        Gdx.gl20 = mock(GL20.class);
        Gdx.gl = mock(GL20.class);
        new HeadlessApplication(applicationListener, cfg);
        level1 = new Level1();
        map = level1.map();
        world = new World(new WorldDef(new Vector2(0, -9.81f)));
        model = new MarioModel();
    }

    @Test
    void testBodyCreation() {
        Array<Body> bodies = new Array<>();
        assertEquals(world.getBodyCount(), 0);
        Body enemyBody = model.bodyCreation(map, world, "enemies", BodyDef.BodyType.DynamicBody, "enemies");
        bodies.add(enemyBody);
        assertEquals(bodies.get(0).getUserData(), "enemies");
        assertEquals(bodies.get(0).getType(), BodyDef.BodyType.DynamicBody);
        float enemyBodies = map.getLayers().get("enemies").getObjects().getCount();
        assertEquals(world.getBodyCount(), enemyBodies);
        Body obstacleBody = model.bodyCreation(map, world, "obstacle", BodyDef.BodyType.StaticBody, "obstacle");
        bodies.add(obstacleBody);
        float obstacleBodies = map.getLayers().get("obstacle").getObjects().getCount();
        assertEquals(world.getBodyCount(), enemyBodies + obstacleBodies);
        assertTrue(bodies.get(1).getUserData().equals("obstacle"));
    }
}
