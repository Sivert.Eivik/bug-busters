package inf112.skeleton.app;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.headless.HeadlessApplication;
import com.badlogic.gdx.backends.headless.HeadlessApplicationConfiguration;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.WorldDef;
import com.badlogic.gdx.utils.Array;

import inf112.skeleton.app.model.Box2DPlayer;
import inf112.skeleton.app.model.Fireball;
import inf112.skeleton.app.model.Player;
import inf112.skeleton.app.model.PlayerState;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class FireballTest {
    private Fireball fireball;
    private World world;
    private Box2DPlayer box2DPlayer;
    private Player player;
    private Array<Body> fireBallArray;

    @BeforeEach
    void setUp() {
        HeadlessApplicationConfiguration cfg = new HeadlessApplicationConfiguration();
        ApplicationListener applicationListener = new ApplicationAdapter() {
        };
        new HeadlessApplication(applicationListener, cfg);
        Gdx.gl20 = mock(GL20.class);
        Gdx.gl = mock(GL20.class);
        world = new World(new WorldDef(new Vector2(0, -9.81f), true));
        player = mock(Player.class);
        box2DPlayer = new Box2DPlayer(world);
        fireball = new Fireball(world, box2DPlayer.getBody(), player);
        fireBallArray = fireball.fireBallArray;
        fireBallArray.clear();
    }

    @Test
    void testAddFireBall() {
        assertEquals(fireBallArray.size, 0);
        fireball.addFireBall(fireball.getFireBall());
        assertEquals(fireBallArray.size, 1);
    }

    @Test
    void testGetFireBall() {
        Body fireBallBody = fireball.getFireBall();
        assertNotEquals(fireBallBody, null);
    }

    @Test
    void testMoveFireBall() {
        when(player.getPlayerState()).thenReturn(PlayerState.RUNNING_RIGHT);
        fireball = new Fireball(world, box2DPlayer.getBody(), player);
        fireball.moveFireball(box2DPlayer);
        assertEquals(fireball.getFireBall().getLinearVelocity().x, 100);
        fireball.removeFireball(fireball.getFireBall());

        when(player.getPlayerState()).thenReturn(PlayerState.RUNNING_LEFT);
        fireball = new Fireball(world, box2DPlayer.getBody(), player);
        fireball.moveFireball(box2DPlayer);
        assertEquals(fireball.getFireBall().getLinearVelocity().x, -100);
    }

    @Test
    void testRenderFireball() {
        SpriteBatch batch = mock(SpriteBatch.class);
        verify(batch, never()).draw(any(Texture.class), anyFloat(), anyFloat());
        fireball.addFireBall(fireball.getFireBall());
        fireball.renderFireball(fireball, batch);
        verify(batch).draw(any(Texture.class), anyFloat(), anyFloat());
        fireball.removeFireball(fireball.getFireBall());
    }
}
