package inf112.skeleton.app;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.headless.HeadlessApplication;
import com.badlogic.gdx.backends.headless.HeadlessApplicationConfiguration;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.WorldDef;

import inf112.skeleton.app.levels.Level1;
import inf112.skeleton.app.model.Box2DPlayer;

public class Box2DPlayerTest {
    private World world;
    private Box2DPlayer box2DPlayer;
    private TiledMap map;

    @BeforeEach
    void setup() {
        HeadlessApplicationConfiguration cfg = new HeadlessApplicationConfiguration();
        ApplicationListener applicationListener = new ApplicationAdapter() {
        };
        new HeadlessApplication(applicationListener, cfg);
        Gdx.gl20 = mock(GL20.class);
        Gdx.gl = mock(GL20.class);
        world = new World(new WorldDef(new Vector2(0, -9.81f), true));
        box2DPlayer = new Box2DPlayer(world);
        Level1 level1 = new Level1();
        map = level1.map();
    }

    @Test
    void testSetPosition() {
        box2DPlayer.setPosition();
        assertEquals(box2DPlayer.getBody().getPosition(), new Vector2(32, 55));
        box2DPlayer.getBody().setTransform(50, 55, 0);
        assertEquals(box2DPlayer.getBody().getPosition(), new Vector2(50, 55));
        box2DPlayer.setPosition();
        assertEquals(box2DPlayer.getBody().getPosition(), new Vector2(32, 55));
    }

    @Test
    void testInsideWorld() {
        box2DPlayer.setPosition();
        assertTrue(box2DPlayer.insideWorld(world, map));
        assertTrue(map.getProperties().get("width", Integer.class).equals(61));
        box2DPlayer.getBody().setTransform(-100, -50, 0);
        assertFalse(box2DPlayer.insideWorld(world, map));
    }
}
