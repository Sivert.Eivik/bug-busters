package inf112.skeleton.app;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.headless.HeadlessApplication;
import com.badlogic.gdx.backends.headless.HeadlessApplicationConfiguration;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.WorldDef;

import inf112.skeleton.app.levels.Level1;
import inf112.skeleton.app.levels.Level2;
import inf112.skeleton.app.levels.Level3;
import inf112.skeleton.app.levels.LevelHandler;
import inf112.skeleton.app.map.BodyCreation;
import inf112.skeleton.app.model.Box2DPlayer;
import inf112.skeleton.app.model.Enemy;
import inf112.skeleton.app.model.Player;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class LevelHandlerTest {
        private World mockedWorld;
        private Box2DPlayer mockedBox2DPlayer;
        private Enemy mockedEnemy;
        private Player mockedPlayer;
        private BodyCreation mockedBodyCreation;
        private LevelHandler levelHandler;
        private Level1 mockLevel1;
        private Level2 mockLevel2;
        private Level3 mockLevel3;

        @BeforeEach
        void setUp() {
                HeadlessApplicationConfiguration cfg = new HeadlessApplicationConfiguration();
                ApplicationListener applicationListener = new ApplicationAdapter() {
                };
                new HeadlessApplication(applicationListener, cfg);
                Gdx.gl20 = mock(GL20.class);
                Gdx.gl = mock(GL20.class);
                mockedWorld = new World(new WorldDef(new Vector2(0, -9.81f), true));
                mockedBox2DPlayer = mock(Box2DPlayer.class);
                mockedEnemy = mock(Enemy.class);
                mockedPlayer = mock(Player.class);
                mockedBodyCreation = mock(BodyCreation.class);
                levelHandler = new LevelHandler(mockedWorld, mockedBox2DPlayer, mockedEnemy, mockedPlayer,
                                mockedBodyCreation);
                mockLevel1 = new Level1();
                mockLevel2 = new Level2();
                mockLevel3 = new Level3();
        }

        @Test
        void testLoadLevel() {
                assertEquals(levelHandler.currentMap(), null);
                levelHandler.loadLevel(1);
                assertNotEquals(levelHandler.currentMap(), null);
                assertEquals(levelHandler.currentMap().getProperties().get("height", Integer.class),
                                mockLevel1.map().getProperties().get("height", Integer.class));
                assertEquals(levelHandler.currentMap().getProperties().get("width", Integer.class),
                                mockLevel1.map().getProperties().get("width", Integer.class));
                assertEquals(levelHandler.currentMap().getLayers().get("background").getObjects().getCount(),
                                mockLevel1.map().getLayers().get("background").getObjects().getCount());
                assertEquals(levelHandler.currentMap().getLayers().get("obstacle").getObjects().getCount(),
                                mockLevel1.map().getLayers().get("obstacle").getObjects().getCount());
                assertEquals(levelHandler.currentMap().getLayers().get("enemies").getObjects().getCount(),
                                mockLevel1.map().getLayers().get("enemies").getObjects().getCount());

                levelHandler.loadLevel(2);
                assertNotEquals(levelHandler.currentMap(), null);
                assertEquals(levelHandler.currentMap().getProperties().get("height", Integer.class),
                                mockLevel2.map().getProperties().get("height", Integer.class));
                assertEquals(levelHandler.currentMap().getProperties().get("width", Integer.class),
                                mockLevel2.map().getProperties().get("width", Integer.class));
                assertEquals(levelHandler.currentMap().getLayers().get("background").getObjects().getCount(),
                                mockLevel2.map().getLayers().get("background").getObjects().getCount());
                assertEquals(levelHandler.currentMap().getLayers().get("obstacle").getObjects().getCount(),
                                mockLevel2.map().getLayers().get("obstacle").getObjects().getCount());
                assertEquals(levelHandler.currentMap().getLayers().get("enemies").getObjects().getCount(),
                                mockLevel2.map().getLayers().get("enemies").getObjects().getCount());

                levelHandler.loadLevel(3);
                assertNotEquals(levelHandler.currentMap(), null);
                assertEquals(levelHandler.currentMap().getProperties().get("height", Integer.class),
                                mockLevel3.map().getProperties().get("height", Integer.class));
                assertEquals(levelHandler.currentMap().getProperties().get("width", Integer.class),
                                mockLevel3.map().getProperties().get("width", Integer.class));
                assertEquals(levelHandler.currentMap().getLayers().get("background").getObjects().getCount(),
                                mockLevel3.map().getLayers().get("background").getObjects().getCount());
                assertEquals(levelHandler.currentMap().getLayers().get("obstacle").getObjects().getCount(),
                                mockLevel3.map().getLayers().get("obstacle").getObjects().getCount());
                assertEquals(levelHandler.currentMap().getLayers().get("enemies").getObjects().getCount(),
                                mockLevel3.map().getLayers().get("enemies").getObjects().getCount());
        }

        @Test
        void testCurrentCameraPosition() {
                levelHandler.currentCameraPosition();
                assertEquals(levelHandler.currentCameraPosition(), new Vector3(310, 160, 0));
        }
}